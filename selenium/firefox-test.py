from selenium import webdriver

# 指定 Firefox 浏览器的驱动程序路径
driver_path = 'C:/Users/roysx/AppData/Local/Programs/Python/Python311/geckodriver.exe'

# 创建 Firefox 浏览器对象
driver = webdriver.Firefox()

# 打开一个网页
driver.get('https://www.baidu.com')

# 在控制台输出网页标题
print(driver.title)

# 关闭浏览器
# driver.quit()
