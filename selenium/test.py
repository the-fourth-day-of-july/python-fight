from selenium import webdriver
from selenium.webdriver.firefox.options import Options

# 指定 Firefox 浏览器的驱动程序路径
#river_path = 'C:/Users/roysx/AppData/Local/Programs/Python/Python311/geckodriver.exe'

# 创建一个 Firefox 配置实例
firefox_options = webdriver.FirefoxOptions()

# 设置请求头信息
#firefox_options.set_capability("moz:firefoxOptions", {"extraHeaders": {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/118.0"}})
firefox_options.add_argument("-headless")
firefox_options.add_argument('blink-settings=imagesEnabled=false')  # 不加载图片, 提升速度

service = webdriver.ChromeService()

# 创建 Firefox 浏览器对象
driver = webdriver.Firefox(options=firefox_options,service=service)

# 打开一个网页
driver.get('https://www.baidu.com')

# 在控制台输出网页标题
print(driver.title)
print("已成功启动")

# 关闭浏览器
driver.quit()
