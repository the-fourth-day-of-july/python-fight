# import requests
# from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By

options = Options()
# 配置火狐选项
options.set_preference("dom.webdriver.enabled", False)  # 禁用火狐对 WebDriver 的检测


headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/117.0",
    "Cookie": "_uab_collina=169530127357918894364617; JSESSIONID=D18487E54A8883D0A1D28927CEEAE3C0; tk=w_vZhpPNvA0F4jihVnxZ4_3edEcB4llxkqYtmgrwr1r0; BIGipServerotn=2698445066.64545.0000; guidesStatus=off; highContrastMode=defaltMode; cursorStatus=off; BIGipServerpassport=854065418.50215.0000; fo=br30ha17te5f5o539HQT_mcNQwZiQbqrp3fn2VoLJ3M1dRAee3HdKQ_s7V6VDFzl14Had_LCebWLNSCG4dmnYMqN4NJyl5xruItMtfbId8t0sxQzJSAX6TxriFMNc5SVQPCHgqOSoM7IdrWs8vXud_wikCl3GMJSzngBCRS_DWqMRZtZnVZil1--Dyc; route=6f50b51faa11b987e576cdb301e545c4; uKey=c3ab57008e7f2281a818913e761eeeb0e044e281f4bb1e4024215262b17adfc4; _jc_save_fromStation=%u5E7F%u5DDE%u5357%2CIZQ; _jc_save_toStation=%u6E5B%u6C5F%u897F%2CZWQ; _jc_save_fromDate=2023-09-28; _jc_save_toDate=2023-09-21; _jc_save_wfdc_flag=dc"
    }

Cookie={
    "_uab_collina": "169530539121220180794019",
    "JSESSIONID": "85E3CBE113AD4F023BEBDE6C82474ACE",
    "tk": "ndXNbSAonlwbW1TjlwOXTM20WgR1q7IGDW_HUA51r1r0",
    "_jc_save_fromStation": "%u5E7F%u5DDE%u5357%2CIZQ",
    "_jc_save_toStation": "%u6E5B%u6C5F%u897F%2CZWQ",
    "_jc_save_fromDate": "2023-09-30",
    "_jc_save_toDate": "2023-09-21",
    "_jc_save_wfdc_flag": "dc",
    "route": "c5c62a339e7744272a54643b3be5bf64",
    "BIGipServerotn": "2581004554.24610.0000",
    "BIGipServerpassport": "954728714.50215.0000",
    "guidesStatus": "off",
    "highContrastMode": "defaltMode",
    "cursorStatus": "off",
    "fo": "aobjdfop3xiixqkiCGIIG-c6VUXR10R8ixGkgnOtUQwjxSO9oazmFbwyrarI-IxcE0NH_uep-U1QJyUzvCSyzndCgsgf7j1jkjhhujIGankwRySDX43HgGXh-eu1KgcDYJziYw3rtiEIscjaBB7oMD-CNXU0xXXSc5xkFYJRCymiK81fqIYxAjTp7MI",
    "uKey": "5f9bb82afd8e5b36f203a78d36930330e9e6e59e419bd88824cdf930d3e4f8a4"
}

# session = requests.Session()
# response = session.get("https://kyfw.12306.cn/otn/leftTicket/init?linktypeid=dc", headers=headers)
# soup = BeautifulSoup(response.content, 'html.parser')
# content=soup.select("#login_user")
# print(content)



driver = webdriver.Firefox()
driver.get('https://kyfw.12306.cn/otn/leftTicket/init?linktypeid=dc')

driver.delete_all_cookies()

# 设置cookies
for cookie_name, cookie_value in Cookie.items():
    driver.add_cookie({'name': cookie_name, 'value': cookie_value})

# 刷新页面，确保cookies生效
driver.refresh()

# 填写起始站
# start_input = driver.find_element_by_id('fromStationText')
# off_input = driver.find_element_by_id('toStationText')
# start_input.send_keys('广州南')
# start_input.send_keys('湛江西')

#提交表单
submit_button = driver.find_element(By.ID, 'query_ticket')
submit_button.click()
