from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By

options = Options()
# 配置火狐选项
options.set_preference("dom.webdriver.enabled", False)  # 禁用火狐对 WebDriver 的检测


Cookie={
   "_uab_collina": "169530539121220180794019",
    "JSESSIONID": "7A2079E89EBD40E1FF4AC3D8184676FC",
    "tk": "mtVOYSnJrFXdXCUJUXaBEvzX7zsjBgL6bmpL8wdqr1r0",
    "_jc_save_fromStation": "%u5E7F%u5DDE%u5357%2CIZQ",
    "_jc_save_toStation": "%u6E5B%u6C5F%u897F%2CZWQ",
    "_jc_save_fromDate": "2023-09-30",
    "_jc_save_toDate": "2023-09-21",
    "_jc_save_wfdc_flag": "dc",
    "guidesStatus": "off",
    "highContrastMode": "defaltMode",
    "cursorStatus": "off",
    "BIGipServerotn": "3973513482.24610.0000",
    "route": "495c805987d0f5c8c84b14f60212447d",
    "BIGipServerpassport": "887619850.50215.0000",
    "fo": "8c7e4pwub08wk3cpfMuBsXTpaJy7lUltS_nBESaDGaRITGZIY3Gl1TtDtlrQ0fMi1Jql82KkentDqye2HudcUcx--MBRMu2gJRiU1inULohy7vo7AiNigy4y9BxgaIVtN_8J1NnOFwfCL4yekcFQayYeByqYGVvSKYzFuOXAnk1n37I14SsBOgPfAOw",
    "uKey": "181f8229845e6401e4fa9fd2bb0599b61f2ade61e79d9ee20618adc07c4f8efb"
}


driver = webdriver.Firefox()
driver.get('https://kyfw.12306.cn/otn/leftTicket/init?linktypeid=dc')

driver.delete_all_cookies()

# 设置cookies
for cookie_name, cookie_value in Cookie.items():
    driver.add_cookie({'name': cookie_name, 'value': cookie_value})

# 刷新页面，确保cookies生效
driver.refresh()

# 填写起始站
start_input = driver.find_element(By.ID, 'fromStationText')
off_input = driver.find_element(By.ID, 'toStationText')
start_input.send_keys('')
off_input.send_keys('')
start_input.send_keys('广州南')
off_input.send_keys('湛江西')

#提交表单
submit_button = driver.find_element(By.ID, 'query_ticket')
submit_button.click()

 # 使用XPath定位下载按钮元素
buy_element = driver.find_element(by='xpath', value='/html/body/div[3]/div[7]/div[9]/table/tbody/tr[13]/td[13]/a')
buy_element.click()
