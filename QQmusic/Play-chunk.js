(window.webpackJsonp = window.webpackJsonp || []).push([[14], {
    376: function(e, t, n) {
        "use strict";
        n.r(t);
        var a = n(2)
          , r = n.n(a)
          , o = n(367)
          , l = n.n(o)
          , i = n(108)
          , s = n.n(i)
          , c = n(78)
          , u = n.n(c)
          , m = n(361)
          , d = n.n(m)
          , p = n(362)
          , y = n.n(p)
          , f = n(149)
          , h = n.n(f)
          , g = n(363)
          , _ = n.n(g)
          , v = n(364)
          , b = n.n(v)
          , E = n(365)
          , L = n.n(E)
          , k = n(76)
          , N = n.n(k)
          , S = n(3)
          , P = n.n(S)
          , w = n(11)
          , x = n(15)
          , T = n(397)
          , C = n(471)
          , D = n(413)
          , O = n(429)
          , M = n(405)
          , j = n(392)
          , I = n(403)
          , A = n(16)
          , R = n(393)
          , U = (n(433),
        n(411))
          , F = n(486);
        function V(e, t) {
            var n = Object.keys(e);
            if (Object.getOwnPropertySymbols) {
                var a = Object.getOwnPropertySymbols(e);
                t && (a = a.filter((function(t) {
                    return Object.getOwnPropertyDescriptor(e, t).enumerable
                }
                ))),
                n.push.apply(n, a)
            }
            return n
        }
        function q(e) {
            for (var t = 1; t < arguments.length; t++) {
                var n = null != arguments[t] ? arguments[t] : {};
                t % 2 ? V(Object(n), !0).forEach((function(t) {
                    r()(e, t, n[t])
                }
                )) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : V(Object(n)).forEach((function(t) {
                    Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t))
                }
                ))
            }
            return e
        }
        var B = function(e) {
            var t = e.config
              , n = e.setConfig
              , a = function(e) {
                n(q(q({}, t), {}, {
                    mod: e
                }))
            };
            return P.a.createElement(P.a.Fragment, null, P.a.createElement("div", {
                className: "form__part"
            }, P.a.createElement("div", {
                className: "form__label"
            }, "\u5386\u53f2\u64ad\u653e\u5217\u8868"), P.a.createElement("span", {
                className: "mod_form_check"
            }, P.a.createElement("label", {
                className: "form_check__label"
            }, P.a.createElement("span", {
                className: "form_check__checkbox ".concat(t.deleteList ? "" : "sprite form_check__checkbox--check")
            }, P.a.createElement("input", {
                type: "checkbox",
                name: "privacy",
                className: "form_check__check",
                onClick: function() {
                    n(q(q({}, t), {}, {
                        deleteList: !t.deleteList
                    }))
                }
            })), "\u4fdd\u7559\uff08\u52fe\u9009\u540e\uff0c\u6bcf\u6b21\u65b0\u6253\u5f00\u64ad\u653e\u5668\uff0c\u5c06\u4fdd\u7559\u4e0a\u6b21\u5173\u95ed\u65f6\u7684\u64ad\u653e\u5217\u8868\uff09"))), P.a.createElement("div", {
                className: "form__part"
            }, P.a.createElement("div", {
                className: "form__label"
            }, "\u64ad\u653e\u8bbe\u7f6e"), P.a.createElement("span", {
                className: "mod_form_radio"
            }, P.a.createElement("label", {
                className: "form_radio__label"
            }, P.a.createElement("span", {
                className: "form_radio__radiobox sprite ".concat(0 === t.mod ? "form_radio__radiobox--check" : "")
            }, P.a.createElement("input", {
                type: "radio",
                name: "privacy",
                className: "form_radio__radio",
                onClick: function() {
                    a(0)
                }
            })), "\u7acb\u5373\u64ad\u653e", P.a.createElement("span", {
                className: "c_tx_thin"
            }, "(\u9ed8\u8ba4\u6dfb\u52a0\u5230\u64ad\u653e\u961f\u5217\u6700\u9876\u90e8)"))), P.a.createElement("span", {
                className: "mod_form_radio"
            }, P.a.createElement("label", {
                className: "form_radio__label"
            }, P.a.createElement("span", {
                className: "form_radio__radiobox sprite ".concat(1 === t.mod ? "form_radio__radiobox--check" : "")
            }, P.a.createElement("input", {
                type: "radio",
                name: "privacy",
                className: "form_radio__radio",
                onClick: function() {
                    a(1)
                }
            })), "\u6dfb\u52a0\u5230\u64ad\u653e\u961f\u5217\u672b\u5c3e")), P.a.createElement("span", {
                className: "mod_form_radio"
            }, P.a.createElement("label", {
                className: "form_radio__label"
            }, P.a.createElement("span", {
                className: "form_radio__radiobox sprite ".concat(2 === t.mod ? "form_radio__radiobox--check" : "")
            }, P.a.createElement("input", {
                type: "radio",
                name: "privacy",
                className: "form_radio__radio",
                onClick: function() {
                    a(2)
                }
            })), "\u4e0b\u4e00\u9996\u64ad\u653e"))))
        }
          , Y = function() {
            var e = Object(S.useState)(F.a.get())
              , t = u()(e, 2)
              , n = t[0]
              , a = t[1]
              , r = Object(S.useState)(!1)
              , o = u()(r, 2)
              , l = o[0]
              , i = o[1];
            return P.a.createElement(P.a.Fragment, null, P.a.createElement("a", {
                className: "player_login__link player_login__link--set",
                onClick: function() {
                    i(!0)
                }
            }, P.a.createElement("span", {
                className: "player_login__txt"
            }, "\u8bbe\u7f6e")), l && P.a.createElement(U.a, {
                visible: !0,
                childrenIsClose: !0,
                width: 600,
                onClose: function() {
                    i(!1)
                }
            }, P.a.createElement(M.a, {
                title: "\u64ad\u653e\u5668\u8bbe\u7f6e",
                topLevelClass: "mod_popup popup_player",
                content: P.a.createElement(B, {
                    config: n,
                    setConfig: a
                }),
                btn: [{
                    fn: function() {
                        i(!1)
                    },
                    text: "\u53d6\u6d88"
                }, {
                    fn: function() {
                        F.a.set(n),
                        i(!1)
                    },
                    text: "\u786e\u5b9a"
                }]
            })))
        }
          , Q = (n(551),
        null)
          , J = function(e) {
            var t = Object(S.useState)(!1)
              , n = u()(t, 2)
              , a = n[0]
              , r = n[1]
              , o = Object(S.useState)(null)
              , l = u()(o, 2)
              , i = l[0]
              , s = l[1];
            Object(S.useEffect)((function() {
                x.d.isBrowser && R.a.isLogin() && R.a.getVipInfo().then((function(t) {
                    var n;
                    s(t),
                    null === (n = e.onLoadedUserInfo) || void 0 === n || n.call(e, t)
                }
                ))
            }
            ), []);
            var c = i && i.base
              , m = x.d.isBrowser && "2" === x.e.getCookie("login_type") ? "//y.qq.com/mediastyle/yqq/img/login_wechat.png?max_age=2592000" : "//y.qq.com/mediastyle/yqq/img/login_qq.png?max_age=2592000";
            return P.a.createElement("div", {
                className: "mod_player_login",
                onMouseEnter: function() {
                    Q && (clearTimeout(Q),
                    Q = null),
                    a || r(!0)
                },
                onMouseLeave: function() {
                    a && (Q = window.setTimeout((function() {
                        r(!1),
                        Q = null
                    }
                    ), 500))
                }
            }, c ? P.a.createElement("a", {
                className: "player_login__link",
                href: Object(A.d)(A.a.PROFILE),
                target: "_blank",
                rel: "noopener noreferrer"
            }, P.a.createElement("img", {
                src: i.base.headurl,
                className: "player_login__cover",
                alt: ""
            }), P.a.createElement("img", {
                src: m,
                className: "player_login__icon"
            }), P.a.createElement("span", {
                className: "player_login__txt"
            }, i.base.nick)) : P.a.createElement("a", {
                className: "player_login__link",
                onClick: function() {
                    R.a.isLogin() || R.a.openLogin()
                }
            }, P.a.createElement("span", {
                className: "player_login__txt"
            }, "\u767b\u5f55")), P.a.createElement(Y, null), c && a && P.a.createElement("a", {
                className: "player_login__out",
                onClick: R.a.loginOut
            }, "\u9000\u51fa"))
        }
          , W = n(402)
          , G = (n(552),
        n(401))
          , z = n(366)
          , H = n.n(z)
          , X = n(444)
          , K = n(408)
          , Z = n(421);
        function $(e) {
            var t = function() {
                if ("undefined" === typeof Reflect || !Reflect.construct)
                    return !1;
                if (Reflect.construct.sham)
                    return !1;
                if ("function" === typeof Proxy)
                    return !0;
                try {
                    return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}
                    ))),
                    !0
                } catch (e) {
                    return !1
                }
            }();
            return function() {
                var n, a = L()(e);
                if (t) {
                    var r = L()(this).constructor;
                    n = Reflect.construct(a, arguments, r)
                } else
                    n = a.apply(this, arguments);
                return b()(this, n)
            }
        }
        var ee = function(e) {
            _()(n, e);
            var t = $(n);
            function n() {
                var e, a;
                d()(this, n);
                for (var r = arguments.length, o = new Array(r), l = 0; l < r; l++)
                    o[l] = arguments[l];
                return b()(a, (e = a = t.call.apply(t, [this].concat(o)),
                a.handleLyricMouseWheel = function(e) {
                    e.preventDefault(),
                    e.stopPropagation()
                }
                ,
                e))
            }
            return y()(n, [{
                key: "componentDidMount",
                value: function() {
                    document.getElementById("qrc_ctn").addEventListener("wheel", this.handleLyricMouseWheel, {
                        passive: !1
                    })
                }
            }, {
                key: "componentWillUnmount",
                value: function() {
                    document.getElementById("qrc_ctn").removeEventListener("wheel", this.handleLyricMouseWheel)
                }
            }, {
                key: "render",
                value: function() {
                    var e, t, n, a, r, o, l, i = this.props, s = i.song, c = i.lyric, u = i.getPlayLyricElment, m = i.translateY, d = i.isTrans, p = i.openLang, y = i.currentTime, f = i.showTransBtn, h = i.handleDragLyricMouseDown;
                    return P.a.createElement("div", {
                        className: "mod_song_info"
                    }, P.a.createElement("div", {
                        className: "song_info__info"
                    }, P.a.createElement("a", {
                        className: "song_info__cover",
                        href: null !== s && void 0 !== s && null !== (e = s.album) && void 0 !== e && e.mid ? Object(A.d)(A.a.ALBUM_DETAIL, null === s || void 0 === s || null === (t = s.album) || void 0 === t ? void 0 : t.mid) : "",
                        target: "_blank",
                        rel: "noopener noreferrer"
                    }, P.a.createElement(W.a, {
                        noFixUrl: !0,
                        imgurl: Object(Z.d)(s, "//y.qq.com/mediastyle/yqq/extra/player_cover.png?max_age=31536000"),
                        type: "album",
                        name: s && s.name,
                        className: "song_info__pic"
                    })), s && P.a.createElement(S.Fragment, null, P.a.createElement("div", {
                        className: "song_info__name"
                    }, "\u6b4c\u66f2\u540d\uff1a", P.a.createElement("a", {
                        href: Object(K.d)({
                            id: s.id,
                            mid: s.mid,
                            songtype: s.type
                        }),
                        target: "_blank",
                        rel: "noopener noreferrer"
                    }, s.title)), P.a.createElement("div", {
                        className: "song_info__singer"
                    }, (null === (n = s.singer) || void 0 === n ? void 0 : n.length) > 0 && (null === (a = s.singer[0]) || void 0 === a ? void 0 : a.id) > 0 && P.a.createElement(P.a.Fragment, null, "\u6b4c\u624b\uff1a", P.a.createElement(G.a, {
                        singers: s.singer
                    }))), P.a.createElement("div", {
                        className: "song_info__album"
                    }, (null === s || void 0 === s || null === (r = s.album) || void 0 === r ? void 0 : r.id) > 0 && (null === s || void 0 === s || null === (o = s.album) || void 0 === o ? void 0 : o.name) && P.a.createElement(P.a.Fragment, null, "\u4e13\u8f91\uff1a", P.a.createElement("a", {
                        href: Object(A.d)(A.a.ALBUM_DETAIL, (null === s || void 0 === s || null === (l = s.album) || void 0 === l ? void 0 : l.mid) || ""),
                        target: "_blank",
                        rel: "noopener noreferrer"
                    }, s.album.name))))), P.a.createElement("div", {
                        className: "song_info__lyric"
                    }, P.a.createElement("div", {
                        className: "song_info__lyric_box",
                        id: "js_lyric_box"
                    }, P.a.createElement("div", {
                        className: "song_info__lyric_inner",
                        style: {
                            transition: "transform 0.1s ease-out",
                            transform: "translateY(-".concat(m, "px)")
                        },
                        id: "qrc_ctn",
                        onMouseDown: h
                    }, c && c.map((function(e, t) {
                        var n = Object(X.c)(e, t, c, y);
                        return P.a.createElement("p", {
                            className: H()({
                                on: n
                            }),
                            key: t,
                            ref: function(e) {
                                n && u(e)
                            }
                        }, e.context)
                    }
                    ))))), f && P.a.createElement("a", {
                        onClick: p,
                        className: H()("btn_lang", {
                            "btn_lang--select": d
                        })
                    }, P.a.createElement("i", {
                        className: "icon_txt"
                    }, "\u5f00\u542f\u97f3\u8bd1\u6b4c\u8bcd")))
                }
            }]),
            n
        }(S.PureComponent)
          , te = n(410)
          , ne = (n(553),
        {
            album: !1,
            time: !0,
            singer: !0,
            rank: !1,
            cover: !1,
            edit: !0,
            more: !1,
            delete: !0
        })
          , ae = function(e) {
            var t = e.playSong
              , n = e.songList
              , a = e.currentMid
              , r = e.changePlay
              , o = e.deleteSong
              , l = e.lyric
              , i = e.currentTime
              , s = e.getPlayLyricElment
              , c = e.translateY
              , m = e.isTrans
              , d = e.showTransBtn
              , p = e.openLang
              , y = e.changeSelect
              , f = e.changeAddSongPlayList
              , h = e.bacthDelete
              , g = e.bacthAddLick
              , _ = e.bacthFan
              , v = e.dowLoadSong
              , b = e.clearSongList
              , E = e.handleDragLyricMouseDown
              , L = P.a.createRef()
              , k = Object(S.useState)(null)
              , N = u()(k, 2)
              , w = N[0]
              , T = N[1]
              , C = function() {
                var e = document.getElementsByClassName("player__bd");
                e && e.length > 0 && T(e[0].clientHeight - 56)
            };
            return Object(S.useEffect)((function() {
                return C(),
                window.addEventListener("resize", C, !1),
                function() {
                    window.removeEventListener("resize", C)
                }
            }
            ), []),
            P.a.createElement("div", {
                className: "player_style_normal"
            }, P.a.createElement("div", {
                className: "mod_songlist_toolbar"
            }, P.a.createElement("a", {
                onClick: g,
                className: "mod_btn"
            }, P.a.createElement("i", {
                className: H()("mod_btn_green__icon_like", {
                    "mod_btn_green__icon_like--like": _
                })
            }), "\u6536\u85cf", P.a.createElement("span", {
                className: "mod_btn__border"
            })), P.a.createElement("a", {
                onClick: function(t) {
                    (0,
                    e.addPlayList)(x.k.getEventPostion(t))
                },
                className: "mod_btn"
            }, P.a.createElement("i", {
                className: "mod_btn_green__icon_add"
            }), "\u6dfb\u52a0\u5230", P.a.createElement("span", {
                className: "mod_btn__border"
            })), P.a.createElement("a", {
                onClick: v,
                className: "mod_btn"
            }, P.a.createElement("i", {
                className: "mod_btn_green__icon_down"
            }), "\u4e0b\u8f7d", P.a.createElement("span", {
                className: "mod_btn__border"
            })), P.a.createElement("a", {
                onClick: function() {
                    var e;
                    null === L || void 0 === L || null === (e = L.current) || void 0 === e || e.makeEmptySelectList(),
                    h()
                },
                className: "mod_btn"
            }, P.a.createElement("i", {
                className: "mod_btn_green__icon_delete"
            }), "\u5220\u9664", P.a.createElement("span", {
                className: "mod_btn__border"
            })), P.a.createElement("a", {
                onClick: b,
                className: "mod_btn"
            }, P.a.createElement("i", {
                className: "mod_btn_green__icon_clear"
            }), "\u6e05\u7a7a\u5217\u8868", P.a.createElement("span", {
                className: "mod_btn__border"
            }))), P.a.createElement("div", {
                className: "sb_scrollable sb_main sb_viewport"
            }, P.a.createElement("div", {
                className: "sb_overview",
                style: {
                    height: w
                }
            }, P.a.createElement(te.a, {
                changeAddSongPlayList: f,
                deleteSong: o,
                changePlay: r,
                isPlayList: !0,
                currentMid: a,
                data: n,
                config: ne,
                changeSelect: y,
                ref: L
            }))), P.a.createElement(ee, {
                showTransBtn: d,
                openLang: p,
                isTrans: m,
                translateY: c,
                getPlayLyricElment: s,
                currentTime: i,
                lyric: l,
                song: t,
                handleDragLyricMouseDown: E
            }))
        }
          , re = Object(S.memo)(ae);
        n(554);
        function oe(e) {
            var t = function() {
                if ("undefined" === typeof Reflect || !Reflect.construct)
                    return !1;
                if (Reflect.construct.sham)
                    return !1;
                if ("function" === typeof Proxy)
                    return !0;
                try {
                    return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}
                    ))),
                    !0
                } catch (e) {
                    return !1
                }
            }();
            return function() {
                var n, a = L()(e);
                if (t) {
                    var r = L()(this).constructor;
                    n = Reflect.construct(a, arguments, r)
                } else
                    n = a.apply(this, arguments);
                return b()(this, n)
            }
        }
        var le = function(e) {
            _()(n, e);
            var t = oe(n);
            function n() {
                return d()(this, n),
                t.apply(this, arguments)
            }
            return y()(n, [{
                key: "render",
                value: function() {
                    var e = this.props
                      , t = e.lyric
                      , n = e.currentTime
                      , a = e.getPlayLyricElment
                      , r = e.showTransBtn
                      , o = e.openLang
                      , l = e.isTrans
                      , i = e.translateY
                      , s = e.handleDragLyricMouseDown;
                    return P.a.createElement("div", {
                        className: "player_style_only"
                    }, P.a.createElement("div", {
                        className: "mod_only_lyric"
                    }, P.a.createElement("div", {
                        style: {
                            transition: "transform 0.1s ease-out",
                            transform: "translateY(-".concat(i, "px)")
                        },
                        className: "only_lyric__inner",
                        id: "qrc_ctn",
                        onMouseDown: s
                    }, P.a.createElement("p", {
                        "data-id": "line_null"
                    }, "\xa0"), t && t.map((function(e, r) {
                        var o = Object(X.c)(e, r, t, n);
                        return P.a.createElement("p", {
                            className: H()({
                                on: o
                            }),
                            key: r,
                            ref: function(e) {
                                o && a(e)
                            }
                        }, e.context)
                    }
                    )))), r && P.a.createElement("a", {
                        onClick: o,
                        className: H()("btn_lang", {
                            "btn_lang--select": l
                        })
                    }, P.a.createElement("i", {
                        className: "icon_txt"
                    }, "\u5f00\u542f\u97f3\u8bd1\u6b4c\u8bcd")))
                }
            }]),
            n
        }(S.PureComponent)
          , ie = n(472);
        function se(e) {
            var t = function() {
                if ("undefined" === typeof Reflect || !Reflect.construct)
                    return !1;
                if (Reflect.construct.sham)
                    return !1;
                if ("function" === typeof Proxy)
                    return !0;
                try {
                    return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}
                    ))),
                    !0
                } catch (e) {
                    return !1
                }
            }();
            return function() {
                var n, a = L()(e);
                if (t) {
                    var r = L()(this).constructor;
                    n = Reflect.construct(a, arguments, r)
                } else
                    n = a.apply(this, arguments);
                return b()(this, n)
            }
        }
        var ce = function(e) {
            _()(n, e);
            var t = se(n);
            function n() {
                var e, a;
                d()(this, n);
                for (var r = arguments.length, o = new Array(r), l = 0; l < r; l++)
                    o[l] = arguments[l];
                return b()(a, (e = a = t.call.apply(t, [this].concat(o)),
                a.handleClickprogressPlay = function(e) {
                    e.stopPropagation()
                }
                ,
                a.handleDownloaing = function() {
                    a.props.dowLoadSong()
                }
                ,
                e))
            }
            return y()(n, [{
                key: "render",
                value: function() {
                    var e = this.props
                      , t = e.song
                      , n = e.play
                      , a = e.prevPlay
                      , r = e.nextPlay
                      , o = e.playStatus
                      , l = e.currentTime
                      , i = e.progressTime
                      , s = e.playMouseDown
                      , c = e.seek
                      , u = e.playType
                      , m = e.setPlayType
                      , d = e.addLike
                      , p = e.commentCount
                      , y = e.setPureMode
                      , f = e.isPureMode
                      , h = e.volume
                      , g = e.volumeMouseDown
                      , _ = e.progressDomRef
                      , v = e.volumeProgressDomRef
                      , b = e.setVolume
                      , E = e.setMute
                      , L = e.currentTimeOffset
                      , k = e.progressLoadOffsetLeft
                      , N = void 0 === k ? "0" : k
                      , w = e.isTryPlay
                      , T = void 0 !== w && w
                      , C = e.tryPlayTipsNode
                      , D = 0
                      , O = "0"
                      , M = "0"
                      , j = !1;
                    if (t) {
                        var I = l + L;
                        D = I >= t.interval ? t.interval : I,
                        O = "".concat((l / t.interval * 100).toFixed(2), "%"),
                        M = "".concat((i / t.interval * 100).toFixed(2), "%"),
                        j = t.isFan
                    }
                    return P.a.createElement("div", {
                        className: "player__ft"
                    }, P.a.createElement("a", {
                        onClick: a,
                        className: "btn_big_prev"
                    }, P.a.createElement("span", {
                        className: "icon_txt"
                    }, "\u4e0a\u4e00\u9996")), P.a.createElement("a", {
                        onClick: n,
                        className: H()("btn_big_play", {
                            "btn_big_play--pause": 1 === o
                        })
                    }, P.a.createElement("span", {
                        className: "icon_txt"
                    }, "\u64ad\u653e")), P.a.createElement("a", {
                        onClick: r,
                        className: "btn_big_next"
                    }, P.a.createElement("span", {
                        className: "icon_txt"
                    }, "\u4e0b\u4e00\u9996")), P.a.createElement("div", {
                        className: "player_music"
                    }, t && P.a.createElement(S.Fragment, null, P.a.createElement("div", {
                        className: "player_music__info",
                        style: {
                            paddingRight: T ? 300 : 90
                        }
                    }, P.a.createElement("a", {
                        title: "Back at One",
                        target: "_blank",
                        rel: "noopener noreferrer",
                        href: Object(K.d)({
                            id: t.id,
                            mid: t.mid,
                            songtype: t.type
                        })
                    }, t.title), " ", "- ", P.a.createElement(G.a, {
                        singers: t.singer
                    }), T && (C || P.a.createElement("a", {
                        className: "player_music__try"
                    }, "\u8bd5\u542c\u4e2d\uff0c\u5b8c\u6574\u64ad\u653e\u9700\u8981\u5f00\u901a\u4f1a\u5458"))), P.a.createElement("div", {
                        className: "player_music__time"
                    }, x.k.makePlayTime(D), " / ", t.playTime)), P.a.createElement("div", {
                        ref: _,
                        onClick: c,
                        className: "player_progress"
                    }, P.a.createElement("div", {
                        className: "player_progress__inner"
                    }, P.a.createElement("div", {
                        className: "player_progress__load",
                        style: {
                            width: M,
                            marginLeft: N
                        }
                    }), P.a.createElement("div", {
                        className: "player_progress__play",
                        style: {
                            width: O,
                            marginLeft: N
                        }
                    }, P.a.createElement("i", {
                        onClick: this.handleClickprogressPlay,
                        onMouseDown: s,
                        className: "player_progress__dot"
                    }))))), 0 === u && P.a.createElement("a", {
                        onClick: m,
                        className: "btn_big_style_list",
                        title: "\u5217\u8868\u5faa\u73af"
                    }, P.a.createElement("span", {
                        className: "icon_txt"
                    }, "\u5217\u8868\u5faa\u73af")), 2 === u && P.a.createElement("a", {
                        onClick: m,
                        className: "btn_big_style_random",
                        title: "\u968f\u673a\u64ad\u653e"
                    }, P.a.createElement("span", {
                        className: "icon_txt"
                    }, "\u968f\u673a\u64ad\u653e")), 1 === u && P.a.createElement("a", {
                        onClick: m,
                        className: "btn_big_style_single",
                        title: "\u5355\u66f2\u5faa\u73af"
                    }, P.a.createElement("span", {
                        className: "icon_txt"
                    }, "\u5355\u66f2\u5faa\u73af")), 3 === u && P.a.createElement("a", {
                        onClick: m,
                        className: "btn_big_style_order",
                        title: "\u987a\u5e8f\u5faa\u73af"
                    }, P.a.createElement("span", {
                        className: "icon_txt"
                    }, "\u987a\u5e8f\u5faa\u73af")), P.a.createElement("a", {
                        onClick: d,
                        className: H()("btn_big_like", {
                            "btn_big_like--like": j
                        })
                    }, P.a.createElement("span", {
                        className: "icon_txt"
                    }, "\u559c\u6b22")), P.a.createElement("a", {
                        onClick: this.handleDownloaing,
                        className: "btn_big_down"
                    }, P.a.createElement("span", {
                        className: "icon_txt"
                    }, "\u4e0b\u8f7d")), P.a.createElement("a", {
                        onClick: y,
                        className: H()("btn_big_only", {
                            "btn_big_only--on": f
                        })
                    }, P.a.createElement("span", {
                        className: "icon_txt"
                    }, f ? "\u5173\u95ed\u7eaf\u6d01\u6a21\u5f0f" : "\u6253\u5f00\u7eaf\u6d01\u6a21\u5f0f")), P.a.createElement(ie.a, {
                        songMid: t && t.mid,
                        commentCount: p
                    }), P.a.createElement("div", {
                        onClick: b,
                        className: "player_progress player_voice"
                    }, P.a.createElement("a", {
                        onClick: E,
                        className: H()("btn_big_voice", {
                            "btn_big_voice--no": 0 === h
                        })
                    }, P.a.createElement("span", {
                        className: "icon_txt"
                    }, 0 === h ? "\u6253\u5f00\u58f0\u97f3" : "\u5173\u95ed\u58f0\u97f3")), P.a.createElement("div", {
                        onClick: b,
                        ref: v,
                        className: "player_progress__inner"
                    }, P.a.createElement("div", {
                        className: "player_progress__play",
                        style: {
                            width: "".concat(100 * h, "%")
                        }
                    }, P.a.createElement("i", {
                        onMouseDown: g,
                        className: "player_progress__dot"
                    })))))
                }
            }]),
            n
        }(S.PureComponent)
          , ue = function() {
            var e = s()(N.a.mark((function e(t) {
                var n, a, r, o, l, i, s, c, u = arguments;
                return N.a.wrap((function(e) {
                    for (; ; )
                        switch (e.prev = e.next) {
                        case 0:
                            if (n = u.length > 1 && void 0 !== u[1] && u[1],
                            t && 0 !== t.length) {
                                e.next = 3;
                                break
                            }
                            return e.abrupt("return", null);
                        case 3:
                            return a = (new Date).getUTCMilliseconds(),
                            a = String(Math.round(2147483647 * Math.random()) * a % 1e10),
                            r = [],
                            o = [],
                            l = [],
                            i = 0,
                            t.forEach((function(e) {
                                var t, a, s = !(null !== (t = e.action) && void 0 !== t && t.play) && e.tryPlay;
                                if (n || !s) {
                                    var c, u;
                                    if (r.push(e.mid),
                                    o.push((null === e || void 0 === e ? void 0 : e.type) || 0),
                                    (null === (a = e.action) || void 0 === a || !a.play) && e.tryPlay)
                                        l.push("RS02".concat((null === (c = e.vs) || void 0 === c ? void 0 : c[0]) || (null === (u = e.file) || void 0 === u ? void 0 : u.media_mid), ".mp3"));
                                    e.ctx && (i = 1)
                                }
                            }
                            )),
                            s = x.j.getUin ? x.j.getUin().toString() : "",
                            c = {
                                guid: a,
                                songmid: r,
                                songtype: o,
                                uin: s,
                                loginflag: 1,
                                platform: "20"
                            },
                            i && (c.ctx = i),
                            l.length && (c.filename = l),
                            e.abrupt("return", Object(x.i)().request([{
                                module: "vkey.GetVkeyServer",
                                method: "CgiGetVkey",
                                param: c
                            }]).then((function(e) {
                                try {
                                    return e[0]
                                } catch (t) {}
                            }
                            )));
                        case 15:
                        case "end":
                            return e.stop()
                        }
                }
                ), e)
            }
            )));
            return function(t) {
                return e.apply(this, arguments)
            }
        }()
          , me = function() {
            function e(t) {
                var n = this;
                d()(this, e),
                de.call(this);
                var a = t.songList
                  , r = document.createElement("audio");
                r.preload = "auto",
                r.style.width = "0",
                r.style.height = "0",
                document.body.appendChild(r),
                this.audio = r,
                this.onVkeyLoadCb = t.onVkeyLoadCb,
                this.addPlayList(a),
                this.audio.addEventListener("play", (function() {
                    n.playPromise || (n.playType = 2,
                    n.isPlay = !0,
                    n.onplayCallback.forEach((function(e) {
                        e()
                    }
                    )))
                }
                )),
                this.audio.addEventListener("pause", (function() {
                    n.playType = 1,
                    n.isPlay = !1,
                    n.onpauseCallback.forEach((function(e) {
                        e()
                    }
                    ))
                }
                )),
                this.audio.addEventListener("timeupdate", (function() {
                    var e = n.audio.currentTime;
                    n.ontimeupdateCallback.forEach((function(t) {
                        t(e)
                    }
                    ))
                }
                )),
                this.audio.addEventListener("loadedmetadata", (function() {
                    n.onloadedmetadataCallback.forEach((function(e) {
                        e()
                    }
                    ))
                }
                )),
                this.audio.addEventListener("ended", (function() {
                    n.playType = 0,
                    n.onendedCallback.forEach((function(e) {
                        e()
                    }
                    ))
                }
                )),
                this.audio.addEventListener("progress", (function() {
                    n.onprogressCallback.forEach((function(e) {
                        e(n.audio.buffered)
                    }
                    ))
                }
                )),
                this.audio.addEventListener("error", (function(e) {
                    n.onerrorCallback.forEach((function(t) {
                        t(e, n.audio.error)
                    }
                    ))
                }
                ))
            }
            return y()(e, [{
                key: "loop",
                get: function() {
                    return this.audio.loop
                },
                set: function(e) {
                    this.audio.loop = e
                }
            }, {
                key: "duration",
                get: function() {
                    return this.audio.duration
                }
            }, {
                key: "currentTime",
                get: function() {
                    return this.audio.currentTime
                },
                set: function(e) {
                    this.audio.currentTime = e
                }
            }, {
                key: "volume",
                get: function() {
                    return this.audio.volume
                },
                set: function(e) {
                    "number" === typeof e && e >= 0 && e <= 1 && (this.audio.volume = e)
                }
            }, {
                key: "curVkeyUrl",
                get: function() {
                    return this.playUrlMap[this.currentMid]
                }
            }, {
                key: "curVkeyCode",
                get: function() {
                    return this.playUrlCodeMap[this.currentMid]
                }
            }, {
                key: "play",
                value: function(e, t) {
                    var n, a = this;
                    if (!e)
                        return 2 === this.playType ? void this.pause() : this.audio.src ? void this.audio.play() : void 0;
                    var r = e.mid;
                    this.currentMid !== e.mid && (this.currentMid = r),
                    isNaN(null === (n = this.audio) || void 0 === n ? void 0 : n.duration) || (this.audio.currentTime = 0);
                    var o = this.playUrlMap[r];
                    o ? (this.audio.src = o,
                    t || this.audio.play()) : this.addPlayList([e], !0).then((function(e) {
                        o = e[r] || "",
                        a.audio.src = o,
                        a.audio.play()
                    }
                    ))
                }
            }, {
                key: "pause",
                value: function() {
                    this.audio.pause()
                }
            }, {
                key: "onplay",
                value: function(e) {
                    this.onplayCallback.push(e)
                }
            }, {
                key: "onpause",
                value: function(e) {
                    this.onpauseCallback.push(e)
                }
            }, {
                key: "ontimeupdate",
                value: function(e) {
                    this.ontimeupdateCallback.push(e)
                }
            }, {
                key: "onprogress",
                value: function(e) {
                    this.onprogressCallback.push(e)
                }
            }, {
                key: "onloadedmetadata",
                value: function(e) {
                    this.onloadedmetadataCallback.push(e)
                }
            }, {
                key: "onended",
                value: function(e) {
                    this.onendedCallback.push(e)
                }
            }, {
                key: "onerror",
                value: function(e) {
                    this.onerrorCallback.push(e)
                }
            }, {
                key: "clear",
                value: function() {
                    this.pause(),
                    this.audio.currentTime = 0,
                    this.audio.src = "",
                    this.currentMid = null,
                    this.playType = 0
                }
            }, {
                key: "ldealUrl",
                value: function(e) {
                    var t = this;
                    if (e && e.midurlinfo) {
                        var n = e.thirdip && e.thirdip[0] ? e.thirdip[0] : "https://dl.stream.qqmusic.qq.com/";
                        e.midurlinfo.forEach((function(e, a) {
                            if (e) {
                                var r = e.purl;
                                r && !/^https?:\/\//i.test(r) && (r = n + r),
                                t.playUrlMap[e.songmid] = r
                            }
                        }
                        ))
                    }
                }
            }, {
                key: "ldealUrlCode",
                value: function(e, t) {
                    var n = this;
                    e.forEach((function(e) {
                        n.playUrlCodeMap[e.mid] = null === t || void 0 === t ? void 0 : t.code
                    }
                    ))
                }
            }]),
            e
        }()
          , de = function() {
            var e = this;
            this.onplayCallback = [],
            this.onendedCallback = [],
            this.onpauseCallback = [],
            this.onloadedmetadataCallback = [],
            this.ontimeupdateCallback = [],
            this.onprogressCallback = [],
            this.onerrorCallback = [],
            this.playType = 0,
            this.playUrlMap = {},
            this.playUrlCodeMap = {},
            this.addPlayList = function(t) {
                var n = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                return ue(t, n).then((function(n) {
                    var a;
                    return e.ldealUrl(null === n || void 0 === n ? void 0 : n.data),
                    e.ldealUrlCode(t, n),
                    null === (a = e.onVkeyLoadCb) || void 0 === a || a.call(e, e.playUrlMap),
                    e.playUrlMap
                }
                ))
            }
            ,
            this.destroy = function() {
                document.body.removeChild(e.audio)
            }
        }
          , pe = me
          , ye = n(394)
          , fe = n(150)
          , he = n.n(fe)
          , ge = ""
          , _e = function e(t) {
            var n = this;
            d()(this, e),
            this.limitNum = 5,
            this.statList = [],
            this.add = function(e) {
                var t = e.songobj
                  , a = e.curTime
                  , r = {
                    id: 0,
                    type: 0,
                    playtime: 0,
                    starttime: 0,
                    fromtag2: 0,
                    fromtag1: 0,
                    err: 0
                };
                r.playtime = a || 0,
                r.err = 0,
                "object" === he()(t) && null != t ? (r.id = t.id,
                r.fromtag1 = 10050,
                r.fromtag2 = t.id,
                t.mid && 0 === t.type ? r.type = 3 : r.type = 1,
                r.starttime = Math.floor((new Date).getTime() / 1e3),
                n.statList.push(r),
                n.statList.length >= n.limitNum && n.submit()) : n.submit()
            }
            ,
            this.submit = function() {
                for (var e = null, t = [], a = [], r = [], o = [], l = [], i = [], s = [], c = n.statList.length, u = 0; u < c; u++)
                    e = n.statList[u],
                    t.push((e.id || 0) < 1 ? 0 : e.id || 0),
                    a.push(e.type || 0),
                    r.push(e.playtime || 0),
                    o.push(e.starttime || 0),
                    l.push(e.fromtag1 || 0),
                    i.push(e.fromtag2 || 0),
                    s.push(e.err || 0);
                if (c > 0) {
                    var m = "//stat.y.qq.com/pc/fcgi-bin/cgi_music_webreport.fcg?Count=".concat(c, "&Fqq=").concat(x.j.getUin(), "&Fguid=").concat(function() {
                        if (ge)
                            return ge;
                        var e = x.e.getCookie("pgv_pvid");
                        if (e && e.length > 0)
                            return ge = e;
                        var t = (new Date).getUTCMilliseconds();
                        return ge = (Math.round(2147483647 * Math.random()) * t % 1e10).toString(),
                        x.e.setCookie("pgv_pvid", ge, null, null, 7200),
                        ge
                    }(), "&Ffromtag1=").concat(l.join(l.join(",")), "&Ffromtag2=").concat(i.join(","), "&Fsong_id=").concat(t.join(","), "&Fplay_time=").concat(r.join(r.join(",")), "&Fstart_time=").concat(o.join(","), "&Ftype=").concat(a.join(a.join(",")), "&Fversion=").concat(1, "&Fid1=").concat(s.join(","));
                    (new Image).src = m
                }
                n.statList = []
            }
            ,
            this.limitNum = t.limitNum || 5
        }
          , ve = (n(555),
        n(453),
        n(450),
        n(477));
        function be(e, t) {
            var n = Object.keys(e);
            if (Object.getOwnPropertySymbols) {
                var a = Object.getOwnPropertySymbols(e);
                t && (a = a.filter((function(t) {
                    return Object.getOwnPropertyDescriptor(e, t).enumerable
                }
                ))),
                n.push.apply(n, a)
            }
            return n
        }
        function Ee(e) {
            for (var t = 1; t < arguments.length; t++) {
                var n = null != arguments[t] ? arguments[t] : {};
                t % 2 ? be(Object(n), !0).forEach((function(t) {
                    r()(e, t, n[t])
                }
                )) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : be(Object(n)).forEach((function(t) {
                    Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t))
                }
                ))
            }
            return e
        }
        function Le(e) {
            var t = function() {
                if ("undefined" === typeof Reflect || !Reflect.construct)
                    return !1;
                if (Reflect.construct.sham)
                    return !1;
                if ("function" === typeof Proxy)
                    return !0;
                try {
                    return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}
                    ))),
                    !0
                } catch (e) {
                    return !1
                }
            }();
            return function() {
                var n, a = L()(e);
                if (t) {
                    var r = L()(this).constructor;
                    n = Reflect.construct(a, arguments, r)
                } else
                    n = a.apply(this, arguments);
                return b()(this, n)
            }
        }
        var ke = {}
          , Ne = function(e) {
            var t = e.func;
            return P.a.createElement("a", {
                className: "player_music__try",
                onClick: t
            }, "\u8bd5\u542c\u4e2d\uff0c\u5b8c\u6574\u64ad\u653e\u9700\u8981\u5f00\u901a\u4f1a\u5458")
        }
          , Se = function(e) {
            _()(n, e);
            var t = Le(n);
            function n() {
                var e, a;
                d()(this, n);
                for (var r = arguments.length, o = new Array(r), i = 0; i < r; i++)
                    o[i] = arguments[i];
                return b()(a, (e = a = t.call.apply(t, [this].concat(o)),
                a.state = {
                    isPureMode: !1,
                    playSong: null,
                    songList: [],
                    playStatus: 0,
                    currentTime: 0,
                    progressTime: 0,
                    playType: 0,
                    currentMid: null,
                    maskImage: null,
                    lyric: null,
                    translateY: 0,
                    isTrans: !0,
                    showTransBtn: !1,
                    currentIndex: 0,
                    bacthFan: !1,
                    commentCount: null,
                    volume: 1
                },
                a.listenReport = new _e({
                    limitNum: 1
                }),
                a.playTypeEnum = [0, 1, 2, 3],
                a.hasInteract = !1,
                a.down = !1,
                a.currentTime = 0,
                a.oldClientX = 0,
                a.currentIndex = -1,
                a.selectArr = [],
                a.volume = 1,
                a.lyricRollValve = !1,
                a.originalY = 0,
                a.originalTop = 0,
                a.timerValve = null,
                a.unblock = null,
                a.onerrorCallback = [],
                a.isShouldAlert = !1,
                a.tryPlayBeginTime = 0,
                a.progressLoadOffsetLeft = "0",
                a.shuffeSonglist = [],
                a.shuffeSongIndex = -1,
                a.handleYMod = function() {
                    var e = (x.g.get("y_mod") || "").split("|").map((function(e) {
                        return parseFloat(e) ? parseFloat(e) : void 0
                    }
                    ))
                      , t = u()(e, 3)
                      , n = t[0]
                      , r = void 0 === n ? 0 : n
                      , o = t[1]
                      , l = void 0 === o ? .5 : o
                      , i = t[2]
                      , s = void 0 === i ? 1 : i;
                    a.playTypeEnum.includes(r) || (r = 0),
                    0 !== l && (l < 0 || l > 1) && (l = .5),
                    [0, 1].includes(s) || (s = 1),
                    a.setState({
                        volume: l,
                        playType: r,
                        isTrans: !!s
                    }),
                    a.volume = l,
                    a.player.volume = l
                }
                ,
                a.handleSetYMod = function(e, t) {
                    if (t) {
                        var n = a.state
                          , r = n.playType
                          , o = n.volume
                          , l = Number(a.state.isTrans);
                        "isTrans" === t && [0, 1].includes(e) ? l = e : "volume" === t && e <= 1 && e >= 0 ? o = e : "playType" === t && a.playTypeEnum.includes(e) && (r = e),
                        x.g.set("y_mod", "".concat(r, "|").concat(o, "|").concat(l))
                    }
                }
                ,
                a.handleIndirectAutoPlay = function() {
                    document.addEventListener("click", (function() {
                        var e;
                        a.hasInteract = !0;
                        var t = (null === (e = a.state.songList) || void 0 === e ? void 0 : e.length) || 0;
                        1 !== a.state.playStatus && t && a.player.play()
                    }
                    ), {
                        once: !0
                    }),
                    setTimeout((function() {
                        var e, t = (null === (e = a.state.songList) || void 0 === e ? void 0 : e.length) || 0;
                        if (1 !== a.state.playStatus && !a.hasInteract && t && a.player.curVkeyUrl)
                            var n = Object(M.b)({
                                title: "QQ\u97f3\u4e50\u63d0\u9192\u60a8",
                                subtit: "\u7531\u4e8e\u60a8\u7684\u6d4f\u89c8\u5668\u8bbe\u7f6e\uff0c\u97f3\u4e50\u65e0\u6cd5\u81ea\u52a8\u64ad\u653e\uff0c\u8bf7\u624b\u52a8\u70b9\u51fb\u64ad\u653e\u3002",
                                type: "warning",
                                btn: [{
                                    fn: function() {
                                        n()
                                    },
                                    text: "\u6211\u77e5\u9053\u4e86",
                                    green: !0
                                }]
                            })
                    }
                    ), 1500)
                }
                ,
                a.getSongLick = function() {
                    var e = s()(N.a.mark((function e(t) {
                        var n;
                        return N.a.wrap((function(e) {
                            for (; ; )
                                switch (e.prev = e.next) {
                                case 0:
                                    return e.next = 2,
                                    Object(T.l)(t);
                                case 2:
                                    (n = e.sent) && a.setState({
                                        songList: n
                                    });
                                case 4:
                                case "end":
                                    return e.stop()
                                }
                        }
                        ), e)
                    }
                    )));
                    return function(t) {
                        return e.apply(this, arguments)
                    }
                }(),
                a.handleChangeAddSongPlayList = function() {
                    var e = a.state.songList;
                    a.getSongLick(e)
                }
                ,
                a.setTranslateY = function() {
                    if (a.lyricElment && !a.lyricRollValve) {
                        var e = a.state.isPureMode
                          , t = e ? 290 : 102
                          , n = a.lyricElment.offsetTop
                          , r = 0;
                        n >= t && (r = n - t),
                        (r += (980 - document.body.clientHeight) / (e ? 5 : 4)) < 0 && (r = 0),
                        a.setState({
                            translateY: r
                        })
                    }
                }
                ,
                a.handleDragLyricMouseDown = function(e) {
                    e.preventDefault(),
                    e.stopPropagation(),
                    a.originalTop || (a.originalTop = a.state.translateY),
                    a.originalY = e.pageY,
                    a.lyricRollValve = !0,
                    document.addEventListener("mouseup", a.handleDragLyricMouseUp),
                    document.addEventListener("mousemove", a.handleDragLyricMouseMove)
                }
                ,
                a.handleDragLyricMouseUp = function() {
                    var e = a.state.translateY;
                    a.originalTop = e < 0 ? 0 : e,
                    a.timerValve && clearTimeout(a.timerValve),
                    a.timerValve = setTimeout((function() {
                        a.lyricRollValve = !1,
                        a.originalTop = 0
                    }
                    ), 3e3),
                    document.removeEventListener("mousemove", a.handleDragLyricMouseMove),
                    document.removeEventListener("mouseup", a.handleDragLyricMouseUp)
                }
                ,
                a.handleDragLyricMouseMove = function(e) {
                    var t = a.originalY - e.pageY + a.originalTop;
                    a.setState({
                        translateY: t < 0 ? 0 : t
                    })
                }
                ,
                a.handleOpenLang = function() {
                    if (-1 !== a.currentIndex) {
                        var e = a.state.isTrans
                          , t = null;
                        t = e ? a.lyricData.lyricList : a.lyricData.transList,
                        a.setState({
                            isTrans: !e,
                            lyric: t
                        }, a.setTranslateY),
                        a.handleSetYMod(Number(!e), "isTrans")
                    }
                }
                ,
                a.setBackground = function(e, t) {
                    var n = x.k.getAlbumPic(e);
                    a.setState({
                        maskImage: n
                    }, (function() {
                        document.body.style.backgroundColor = "#".concat(t)
                    }
                    ))
                }
                ,
                a.checkPlay = function(e, t, n) {
                    var r, o = a.state.songList, l = e[t];
                    if (l) {
                        if (-1 !== a.currentIndex && (null === (r = o[a.currentIndex]) || void 0 === r ? void 0 : r.mid) === (null === l || void 0 === l ? void 0 : l.mid))
                            return n || 2 === a.player.playType || a.player.play(),
                            a.currentIndex = t,
                            void a.setState({
                                currentIndex: t
                            });
                        Object(C.a)({
                            mid: l.mid,
                            songid: l.id,
                            songtype: l.type
                        }).then((function(e) {
                            a.lyricData = e;
                            try {
                                var t = e.transList && e.transList.length > 0;
                                a.setState({
                                    showTransBtn: t,
                                    isTrans: !0,
                                    lyric: t ? e.transList : e.lyricList
                                })
                            } catch (n) {}
                        }
                        )),
                        Object(T.f)([{
                            id: l.id,
                            type: l.type
                        }]).then((function(e) {
                            a.setState({
                                commentCount: e[l.id] || null
                            })
                        }
                        ));
                        var i = l.album.mid;
                        ke[i] ? a.setBackground(i, ke[i]) : Object(T.d)(l.album.mid).then((function(e) {
                            ke[i] = e.color,
                            a.setBackground(i, e.color)
                        }
                        )),
                        a.currentIndex = t,
                        a.tryPlayBeginTime = 0,
                        a.progressLoadOffsetLeft = "0",
                        a.setState({
                            progressTime: 0,
                            currentTime: 0,
                            currentIndex: t,
                            translateY: 0,
                            lyric: null,
                            commentCount: null,
                            volume: a.volume
                        }),
                        a.handleTryPlayTime(l),
                        a.player.play(l, n),
                        a.playSongData.index = t,
                        x.g.setJson("playSongData", a.playSongData)
                    } else
                        a.handleClearAllResetState()
                }
                ,
                a.handleTryPlayTime = function(e) {
                    var t;
                    null !== (t = e.action) && void 0 !== t && t.play || !e.tryPlay || (a.tryPlayBeginTime = parseInt("".concat(e.file.b_30s / 1e3), 10),
                    a.progressLoadOffsetLeft = "".concat((a.tryPlayBeginTime / e.interval * 100).toFixed(2), "%"),
                    a.handleTryPlayAlert(e))
                }
                ,
                a.handleTryPlayAlert = function(e) {
                    a.isShouldAlert && (Object(D.a)(e),
                    a.isShouldAlert = !1,
                    a.playSongData.payAlertTimestamp = (new Date).valueOf())
                }
                ,
                a.handleTryPlayAlertOrNot = function() {
                    var e = a.playSongData.payAlertTimestamp
                      , t = Object(K.g)((new Date).valueOf())
                      , n = Object(K.g)(e);
                    a.isShouldAlert = !n || t > n
                }
                ,
                a.handleChangePlay = function(e, t, n, r) {
                    var o = t || a.state.songList;
                    a.currentIndex !== e || n ? (a.listenReport.add({
                        songobj: a.state.songList[a.state.currentIndex],
                        curTime: Math.floor(a.state.currentTime || 0)
                    }),
                    a.checkPlay(o, e, r)) : a.player.play()
                }
                ,
                a.handleRandomPlay = function(e, t) {
                    var n = a.state.songList
                      , r = Object(X.b)({
                        type: e,
                        songList: n,
                        currentIndex: t || a.currentIndex,
                        shuffeSongIndex: a.shuffeSongIndex,
                        shuffeSonglist: a.shuffeSonglist
                    })
                      , o = r.currentIndex
                      , l = r.shuffeSongIndex
                      , i = r.newShuffeSonglist;
                    return a.shuffeSongIndex = l,
                    i && (a.shuffeSonglist = i),
                    {
                        currentIndex: o
                    }
                }
                ,
                a.handleCurrentPlay = function() {
                    -1 !== a.currentIndex && a.handleChangePlay(a.currentIndex)
                }
                ,
                a.handlePrevPlay = function() {
                    if (-1 !== a.currentIndex && a.state.songList) {
                        var e = a.state
                          , t = e.songList
                          , n = e.playType
                          , r = a.currentIndex;
                        if (2 === n)
                            r = a.handleRandomPlay("prev").currentIndex;
                        else
                            0 === r ? r = t.length - 1 : r--;
                        a.handleChangePlay(r)
                    }
                }
                ,
                a.handleNextPlay = function() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                    if (-1 !== a.currentIndex && a.state.songList) {
                        var t = a.state
                          , n = t.songList
                          , r = t.playType
                          , o = a.currentIndex;
                        if (!e || 3 !== r || o !== n.length - 1) {
                            if (2 === r) {
                                var l = a.handleRandomPlay("next")
                                  , i = l.currentIndex;
                                o = i
                            } else
                                (!e || 0 !== r && 3 !== r) && e || (o === n.length - 1 ? o = 0 : o++);
                            a.handleChangePlay(o)
                        }
                    }
                }
                ,
                a.handleClickSongPlay = function(e, t, n, r) {
                    2 === a.state.playType && a.handleRandomPlay("index", e),
                    a.handleChangePlay(e, t, n, r)
                }
                ,
                a.handleMute = function(e) {
                    e.stopPropagation(),
                    0 !== a.state.volume ? (a.setState({
                        volume: 0
                    }),
                    a.player.volume = 0) : (a.player.volume = a.volume,
                    a.setState({
                        volume: a.volume
                    }))
                }
                ,
                a.handleVolumeMouseUp = function() {
                    window.removeEventListener("mouseup", a.handleVolumeMouseUp, !1),
                    window.removeEventListener("mousemove", a.handleVolumeMousemove, !1),
                    a.player.volume = a.volume
                }
                ,
                a.handleVolumeMousemove = function(e) {
                    e.preventDefault(),
                    e.stopPropagation();
                    var t = Object(X.a)(e, a.volumeProgressDom);
                    a.volume = t,
                    a.setState({
                        volume: t
                    })
                }
                ,
                a.handleVolumeMouseDown = function() {
                    window.addEventListener("mousemove", a.handleVolumeMousemove, !1),
                    window.addEventListener("mouseup", a.handleVolumeMouseUp, !1)
                }
                ,
                a.handleSetVolume = function(e) {
                    var t = Object(X.a)(e, a.volumeProgressDom);
                    a.setState({
                        volume: t
                    }),
                    a.volume = t,
                    a.player.volume = t,
                    a.handleSetYMod(t, "volume")
                }
                ,
                a.handleMousemove = function(e) {
                    e.preventDefault(),
                    e.stopPropagation();
                    var t = a.state.songList
                      , n = Object(X.a)(e, a.progressDom) * t[a.currentIndex].interval;
                    a.setState({
                        currentTime: n
                    })
                }
                ,
                a.handleMouseDown = function(e) {
                    -1 !== a.currentIndex && (e.preventDefault(),
                    e.stopPropagation(),
                    a.down = !0,
                    window.addEventListener("mousemove", a.handleMousemove, !1),
                    window.addEventListener("mouseup", a.handleMouseUp, !1))
                }
                ,
                a.handleMouseUp = function() {
                    window.removeEventListener("mouseup", a.handleMouseUp, !1),
                    window.removeEventListener("mousemove", a.handleMousemove, !1),
                    a.down = !1,
                    a.player.currentTime = a.state.currentTime
                }
                ,
                a.handleSeek = function(e) {
                    if (-1 !== a.currentIndex) {
                        var t = a.state.songList[a.currentIndex];
                        if (!a.isTryPlay) {
                            var n = Object(X.a)(e, a.progressDom) * t.interval;
                            a.setState({
                                currentTime: n
                            }),
                            a.player.currentTime = n
                        }
                    }
                }
                ,
                a.handleSetPlayType = function() {
                    var e = a.state.playType;
                    3 === e ? e = 0 : e++,
                    a.player.loop = 1 === e,
                    a.setState({
                        playType: e
                    }),
                    a.handleSetYMod(e, "playType")
                }
                ,
                a.handleDeleteSong = function(e, t) {
                    var n = !1;
                    if ("[object Array]" === Object.prototype.toString.call(e) && e.length === a.state.songList.length && (n = !0),
                    t || n)
                        a.handleClearAllResetState();
                    else {
                        var r = a.state.songList
                          , o = l()(r)
                          , i = "number" === typeof e ? [e] : e;
                        if (0 !== i.length) {
                            var s = h()(a).currentIndex;
                            if ((o = o.filter((function(e, t) {
                                var n = -1 === i.indexOf(t);
                                return n || s > t && s--,
                                n
                            }
                            )))[s] || (s = 0),
                            0 !== o.length) {
                                var c = 2 !== a.player.playType;
                                a.handleChangePlay(s, o, !0, c),
                                a.playSongData.songList = o,
                                x.g.setJson("playSongData", a.playSongData),
                                a.setState({
                                    songList: o
                                }, a.getbacthFan)
                            } else
                                a.handleClearAllResetState()
                        } else
                            j.a.show({
                                type: "warning",
                                msg: "\u8bf7\u9009\u62e9\u6b4c\u66f2"
                            })
                    }
                }
                ,
                a.getPlayLyricElment = function(e) {
                    a.lyricElment = e
                }
                ,
                a.setListLick = function(e, t, n) {
                    return a.state.songList.map((function(a, r) {
                        var o = a;
                        return -1 !== e.indexOf(r) && ((o = Ee({}, o)).isFan = t,
                        n && n.push(o)),
                        o
                    }
                    ))
                }
                ,
                a.handleAddLike = function() {
                    var e = s()(N.a.mark((function e(t) {
                        var n, r, o, l, i, s;
                        return N.a.wrap((function(e) {
                            for (; ; )
                                switch (e.prev = e.next) {
                                case 0:
                                    if (-1 !== a.currentIndex) {
                                        e.next = 2;
                                        break
                                    }
                                    return e.abrupt("return");
                                case 2:
                                    if (n = a.state,
                                    r = n.songList,
                                    o = n.bacthFan,
                                    l = [],
                                    0 !== (i = Array.isArray(t) ? t : [a.currentIndex]).length) {
                                        e.next = 9;
                                        break
                                    }
                                    return j.a.show({
                                        type: "warning",
                                        msg: "\u8bf7\u9009\u62e9\u6b4c\u66f2"
                                    }),
                                    e.abrupt("return");
                                case 9:
                                    return s = 1 === i.length ? r[i[0]].isFan : o,
                                    a.setState({
                                        songList: a.setListLick(i, !s, l)
                                    }, (function() {
                                        a.selectArr.length > 0 && a.getbacthFan()
                                    }
                                    )),
                                    e.next = 13,
                                    Object(T.a)(201, l, s);
                                case 13:
                                    e.sent || a.setState({
                                        songList: a.setListLick(i, !s)
                                    }, (function() {
                                        a.selectArr.length > 0 && a.getbacthFan()
                                    }
                                    ));
                                case 15:
                                case "end":
                                    return e.stop()
                                }
                        }
                        ), e)
                    }
                    )));
                    return function(t) {
                        return e.apply(this, arguments)
                    }
                }(),
                a.handleChangeSelect = function(e, t) {
                    a.selectSongList = e,
                    a.selectArr = t,
                    a.getbacthFan()
                }
                ,
                a.bacthAddLick = function() {
                    a.handleAddLike(a.selectArr)
                }
                ,
                a.bacthDelete = function() {
                    a.handleDeleteSong(a.selectArr),
                    a.selectArr = []
                }
                ,
                a.progressDomRef = function(e) {
                    a.progressDom = e
                }
                ,
                a.volumeProgressDomRef = function(e) {
                    a.volumeProgressDom = e
                }
                ,
                a.bacthDownloadSong = function() {
                    -1 !== a.currentIndex && (0 !== a.selectArr.length ? a.handleDowLoadSong(a.selectSongList) : j.a.show({
                        type: "warning",
                        msg: "\u8bf7\u9009\u62e9\u6b4c\u66f2"
                    }))
                }
                ,
                a.handleDowLoadSong = function(e) {
                    -1 !== a.currentIndex && Object(D.d)(e || [a.state.songList[a.currentIndex]])
                }
                ,
                a.addPlayList = function(e) {
                    -1 !== a.currentIndex && (0 !== a.selectArr.length ? Object(I.b)(Ee(Ee({}, e), {}, {
                        element: P.a.createElement(O.a, {
                            isPlayList: !0,
                            songList: a.selectSongList
                        }),
                        className: "mod_operate_menu"
                    })) : j.a.show({
                        type: "warning",
                        msg: "\u8bf7\u9009\u62e9\u6b4c\u66f2"
                    }))
                }
                ,
                a.handleSetPureMode = function() {
                    var e = a.state.isPureMode;
                    a.setState({
                        isPureMode: !e
                    }, a.setTranslateY)
                }
                ,
                a.handleClearSongList = function() {
                    var e = a.state.songList;
                    if (e && 0 !== e.length)
                        var t = Object(M.b)({
                            title: "QQ\u97f3\u4e50",
                            msg: "\u786e\u5b9a\u8981\u6e05\u7a7a\u5217\u8868\uff1f",
                            type: "warning",
                            btn: [{
                                fn: function() {
                                    t()
                                },
                                text: "\u53d6\u6d88"
                            }, {
                                fn: function() {
                                    a.handleDeleteSong(null, !0),
                                    t()
                                },
                                text: "\u786e\u8ba4"
                            }]
                        })
                }
                ,
                a.handleClearAllResetState = function() {
                    a.setState({
                        lyric: null,
                        commentCount: null,
                        currentIndex: 0,
                        songList: null,
                        playStatus: 0
                    }),
                    a.currentIndex = -1,
                    a.tryPlayBeginTime = 0,
                    a.progressLoadOffsetLeft = "0",
                    a.player.clear(),
                    x.g.setJson("playSongData", null)
                }
                ,
                a.handleClickTryTips = function() {
                    Object(ve.a)({
                        aid: "music.pc.song.webkt.vip",
                        entry: 132
                    })
                }
                ,
                a.handleUserInfoLoaded = function() {
                    var e = R.a.isVip();
                    a.playSongData.isvip !== e && Object(X.d)(a.playSongData, !1, !0)
                }
                ,
                a.handleVkeyUpdateSonginfo = function(e) {
                    var t = a.state.songList;
                    a.setState({
                        songList: t.map((function(t) {
                            return Ee(Ee({}, t), {}, {
                                disabled: "" === e[t.mid]
                            })
                        }
                        ))
                    })
                }
                ,
                e))
            }
            return y()(n, [{
                key: "componentDidMount",
                value: function() {
                    var e = s()(N.a.mark((function e() {
                        var t, n, a = this;
                        return N.a.wrap((function(e) {
                            for (; ; )
                                switch (e.prev = e.next) {
                                case 0:
                                    return Object(Z.b)(),
                                    window.name = "QQMusicPlayer",
                                    n = x.g.getJson("playSongData") || {},
                                    this.playSongData = n,
                                    e.next = 6,
                                    Object(X.d)(this.playSongData);
                                case 6:
                                    this.setState({
                                        songList: n.songList
                                    }),
                                    this.handleTryPlayAlertOrNot(),
                                    x.g.changeStorage((function(e) {
                                        if ("playSongData" === e.key) {
                                            a.playSongData = e.newValue || {};
                                            var t = a.playSongData
                                              , n = t.songList
                                              , r = t.index;
                                            a.playSongData.isPlay && a.handleChangePlay(r, n, !0),
                                            a.setState({
                                                songList: n
                                            }),
                                            !n && a.state.playStatus && (a.handleCurrentPlay(),
                                            a.currentIndex = -1)
                                        }
                                    }
                                    )),
                                    this.unblock = this.props.history.block((function(e) {
                                        if (a.props.location.pathname.includes(Object(A.d)(A.a.PLAYER)))
                                            return window.open(e.pathname, "_blank"),
                                            !1
                                    }
                                    )),
                                    this.initPlayer(n.songList),
                                    x.h.doPvg(Object(A.d)(A.a.PLAYER)),
                                    null === (t = window.SPD) || void 0 === t || t.init({
                                        flag: [1649, 12, 9]
                                    }),
                                    this.handleIndirectAutoPlay(),
                                    this.handleYMod();
                                case 15:
                                case "end":
                                    return e.stop()
                                }
                        }
                        ), e, this)
                    }
                    )));
                    return function() {
                        return e.apply(this, arguments)
                    }
                }()
            }, {
                key: "componentWillUnmount",
                value: function() {
                    window.name = "",
                    document.body.style.backgroundColor = "#fafafa",
                    this.player.destroy(),
                    this.unblock && this.unblock()
                }
            }, {
                key: "initPlayer",
                value: function(e) {
                    if (this.player = new pe({
                        songList: e,
                        onVkeyLoadCb: this.handleVkeyUpdateSonginfo
                    }),
                    this.volume = this.player.volume,
                    this.initPlayerEvent(),
                    e && e.length > 0) {
                        this.getSongLick(e);
                        var t = this.playSongData.index || 0;
                        e && e.length > 0 && this.handleChangePlay(t, e)
                    }
                }
            }, {
                key: "initPlayerEvent",
                value: function() {
                    var e = this;
                    this.player.onplay((function() {
                        var t = e.state.songList;
                        if (t) {
                            var n = t[e.currentIndex];
                            e.setState({
                                playStatus: 1,
                                currentMid: (null === n || void 0 === n ? void 0 : n.mid) || ""
                            })
                        }
                    }
                    )),
                    this.player.onpause((function() {
                        e.setState({
                            playStatus: 0,
                            currentMid: null
                        })
                    }
                    )),
                    this.player.ontimeupdate((function(t) {
                        e.down || (e.currentTime = t,
                        e.setState({
                            currentTime: e.currentTime
                        }, e.setTranslateY))
                    }
                    )),
                    this.player.onprogress((function(t) {
                        var n = 0;
                        -1 !== e.currentIndex && t.length && (n = t.end(0)),
                        e.setState({
                            progressTime: n
                        })
                    }
                    )),
                    this.player.onended((function() {
                        e.handleNextPlay(!0)
                    }
                    )),
                    this.player.onerror((function(t, n) {
                        var a = e.state
                          , r = a.currentIndex
                          , o = a.songList;
                        if (n.code === n.MEDIA_ERR_SRC_NOT_SUPPORTED) {
                            var l = e.player
                              , i = l.curVkeyCode
                              , s = l.curVkeyUrl;
                            if (r < (null === o || void 0 === o ? void 0 : o.length) - 1 && setTimeout((function() {
                                e.handleNextPlay()
                            }
                            ), 2e3),
                            "number" === typeof i && "" === s)
                                return void j.a.show({
                                    type: "warning",
                                    msg: "\u64ad\u653e\u5931\u8d25\uff01\u8be5\u6b4c\u66f2\u6682\u65e0\u64ad\u653e\u6743\u9650\uff01"
                                })
                        }
                        null !== o && void 0 !== o && o.length && j.a.show({
                            type: "warning",
                            msg: "\u64ad\u653e\u5931\u8d25\uff01\u5f53\u524d\u7f51\u7edc\u7e41\u5fd9\uff0c\u8bf7\u60a8\u7a0d\u540e\u518d\u8bd5\uff01"
                        })
                    }
                    ))
                }
            }, {
                key: "getbacthFan",
                value: function() {
                    var e = this.selectArr;
                    if (0 !== e.length) {
                        for (var t = this.state.songList, n = 0; n < e.length; n++) {
                            if (!t[e[n]].isFan)
                                return void this.setState({
                                    bacthFan: !1
                                })
                        }
                        this.setState({
                            bacthFan: !0
                        })
                    } else
                        this.setState({
                            bacthFan: !1
                        })
                }
            }, {
                key: "isTryPlay",
                get: function() {
                    var e, t = this.state, n = t.songList, a = t.currentIndex, r = null === n || void 0 === n ? void 0 : n[a];
                    return !(null !== r && void 0 !== r && null !== (e = r.action) && void 0 !== e && e.play) && !(null === r || void 0 === r || !r.tryPlay)
                }
            }, {
                key: "render",
                value: function() {
                    var e = this.state
                      , t = e.isPureMode
                      , n = e.songList
                      , a = e.maskImage
                      , r = e.playStatus
                      , o = e.currentMid
                      , l = e.currentTime
                      , i = e.progressTime
                      , s = e.playType
                      , c = e.lyric
                      , u = e.translateY
                      , m = e.isTrans
                      , d = e.showTransBtn
                      , p = e.currentIndex
                      , y = e.bacthFan
                      , f = e.commentCount
                      , h = e.volume
                      , g = n && n[p];
                    return P.a.createElement(S.Fragment, null, P.a.createElement("h1", {
                        className: "player_logo"
                    }, P.a.createElement("a", {
                        href: "/",
                        target: "_blank"
                    }, P.a.createElement("img", {
                        className: "player_logo__pic",
                        srcSet: "//y.qq.com/mediastyle/yqq/img/player_logo@2x.png?max_age=2592000 2x",
                        src: "//y.qq.com/mediastyle/yqq/img/player_logo.png?max_age=2592000",
                        alt: "QQ\u97f3\u4e50"
                    }))), P.a.createElement("div", {
                        className: "bg_player_mask"
                    }), P.a.createElement("div", {
                        className: "bg_player",
                        style: {
                            display: a ? "block" : "none",
                            backgroundImage: "url(".concat(a, ")"),
                            backgroundColor: "#fff"
                        }
                    }), P.a.createElement(J, {
                        onLoadedUserInfo: this.handleUserInfoLoaded
                    }), P.a.createElement("div", {
                        className: "mod_player"
                    }, P.a.createElement("div", {
                        className: "player__hd"
                    }), P.a.createElement("div", {
                        className: "player__bd"
                    }, t ? P.a.createElement(le, {
                        translateY: u,
                        showTransBtn: d,
                        openLang: this.handleOpenLang,
                        isTrans: m,
                        getPlayLyricElment: this.getPlayLyricElment,
                        currentTime: l,
                        lyric: c,
                        handleDragLyricMouseDown: this.handleDragLyricMouseDown
                    }) : P.a.createElement(re, {
                        clearSongList: this.handleClearSongList,
                        addPlayList: this.addPlayList,
                        dowLoadSong: this.bacthDownloadSong,
                        bacthFan: y,
                        bacthAddLick: this.bacthAddLick,
                        bacthDelete: this.bacthDelete,
                        changeSelect: this.handleChangeSelect,
                        changeAddSongPlayList: this.handleChangeAddSongPlayList,
                        showTransBtn: d,
                        openLang: this.handleOpenLang,
                        isTrans: m,
                        translateY: u,
                        getPlayLyricElment: this.getPlayLyricElment,
                        lyric: c,
                        currentTime: l,
                        deleteSong: this.handleDeleteSong,
                        changePlay: this.handleClickSongPlay,
                        playSong: g,
                        currentMid: o,
                        songList: n || [],
                        handleDragLyricMouseDown: this.handleDragLyricMouseDown
                    })), P.a.createElement(ce, {
                        setMute: this.handleMute,
                        volumeMouseDown: this.handleVolumeMouseDown,
                        volume: h,
                        isPureMode: t,
                        setPureMode: this.handleSetPureMode,
                        commentCount: f,
                        dowLoadSong: this.handleDowLoadSong,
                        addLike: this.handleAddLike,
                        prevPlay: this.handlePrevPlay,
                        nextPlay: this.handleNextPlay,
                        setPlayType: this.handleSetPlayType,
                        playType: s,
                        progressDomRef: this.progressDomRef,
                        volumeProgressDomRef: this.volumeProgressDomRef,
                        seek: this.handleSeek,
                        playMouseDown: this.handleMouseDown,
                        progressTime: i,
                        currentTime: l,
                        playStatus: r,
                        play: this.handleCurrentPlay,
                        song: g,
                        setVolume: this.handleSetVolume,
                        currentTimeOffset: this.tryPlayBeginTime,
                        progressLoadOffsetLeft: this.progressLoadOffsetLeft,
                        isTryPlay: this.isTryPlay,
                        tryPlayTipsNode: P.a.createElement(Ne, {
                            func: this.handleClickTryTips
                        })
                    })))
                }
            }]),
            n
        }(S.PureComponent);
        Se.getInitialMetas = s()(N.a.mark((function e() {
            return N.a.wrap((function(e) {
                for (; ; )
                    switch (e.prev = e.next) {
                    case 0:
                        return e.abrupt("return", Object(ye.a)({}));
                    case 1:
                    case "end":
                        return e.stop()
                    }
            }
            ), e)
        }
        )));
        t.default = Object(w.withRouter)(Se)
    },
    444: function(e, t, n) {
        "use strict";
        n.d(t, "c", (function() {
            return p
        }
        )),
        n.d(t, "a", (function() {
            return y
        }
        )),
        n.d(t, "d", (function() {
            return f
        }
        )),
        n.d(t, "b", (function() {
            return h
        }
        ));
        var a = n(367)
          , r = n.n(a)
          , o = n(108)
          , l = n.n(o)
          , i = n(76)
          , s = n.n(i)
          , c = n(397)
          , u = n(408)
          , m = n(393)
          , d = n(15)
          , p = function(e, t, n, a) {
            return null !== e.interval && (t === n.length - 1 ? a >= e.interval : a >= e.interval && a < n[t + 1].interval)
        }
          , y = function(e, t) {
            var n = function(e) {
                for (var t = e.offsetLeft, n = e.offsetParent; null !== n; )
                    t += n.offsetLeft,
                    n = n.offsetParent;
                return t
            }(t)
              , a = (e.clientX - n) / t.offsetWidth;
            return a > 1 ? a = 1 : a < 0 && (a = 0),
            a
        }
          , f = function() {
            var e = l()(s.a.mark((function e(t) {
                var n, a, r, o, l, i, u, p, y, f = arguments;
                return s.a.wrap((function(e) {
                    for (; ; )
                        switch (e.prev = e.next) {
                        case 0:
                            if (n = f.length > 1 && void 0 !== f[1] && f[1],
                            a = f.length > 2 && void 0 !== f[2] && f[2],
                            r = m.a.getUin(),
                            o = m.a.isVip(),
                            (l = r != t.uin || o !== t.isvip) && (t.uin = Number(r) || 0),
                            !n && !l) {
                                e.next = 16;
                                break
                            }
                            return u = [],
                            p = [],
                            t.isvip = o,
                            null === (i = t.songList) || void 0 === i || i.forEach((function(e) {
                                e && (u.push(e.id),
                                p.push(e.type))
                            }
                            )),
                            e.next = 13,
                            Object(c.m)(u, p);
                        case 13:
                            null !== (y = e.sent) && void 0 !== y && y.length && (t.songList = y),
                            a && d.g.setJson("playSongData", t);
                        case 16:
                        case "end":
                            return e.stop()
                        }
                }
                ), e)
            }
            )));
            return function(t) {
                return e.apply(this, arguments)
            }
        }()
          , h = function(e) {
            var t = e.type
              , n = e.songList
              , a = e.currentIndex
              , o = e.shuffeSongIndex
              , l = e.shuffeSonglist
              , i = l.length
              , s = n.length;
            "next" === t ? o += 1 : "prev" === t ? o -= 1 : "index" === t && (o = l.findIndex((function(e) {
                return e.mid === n[a].mid && e.type === n[a].type
            }
            )));
            var c = i <= o || o < 0 || s !== i
              , m = c
              , d = o < 0;
            if (c) {
                var p = r()(n);
                p.splice(a, 1);
                var y = Object(u.e)(p);
                d ? y.push(n[a]) : y.unshift(n[a]),
                l = y,
                o = d ? 0 : 1
            }
            var f = n.findIndex((function(e) {
                return e.mid === l[o].mid && e.type === l[o].type
            }
            ));
            return (f < 0 || f >= n.length) && (f = 0),
            {
                shuffeSongIndex: o,
                currentIndex: f,
                newShuffeSonglist: m ? l : void 0
            }
        }
    },
    453: function(e, t, n) {},
    472: function(e, t, n) {
        "use strict";
        var a = n(3)
          , r = n.n(a)
          , o = n(16)
          , l = (n(453),
        n(473),
        function(e) {
            var t, n = e.songMid, a = e.commentCount, l = function() {
                n ? window.open(Object(o.d)(o.a.SONGE_DETAIL, "".concat(n, "#comment_box"))) : document.getElementById("comment_box").scrollIntoView(!0)
            };
            return null === a || 0 === a ? t = r.a.createElement("a", {
                className: "mod_btn_comment",
                onClick: l
            }, r.a.createElement("span", {
                className: "icon_txt"
            }, "\u65e0\u8bc4\u8bba")) : a > 0 && a < 10 ? t = r.a.createElement("a", {
                className: "mod_btn_comment btn_comment9",
                onClick: l
            }, r.a.createElement("span", {
                className: "btn_comment__numbers"
            }, r.a.createElement("i", {
                className: "btn_comment__numb btn_comment__numb_".concat(a)
            })), r.a.createElement("span", {
                className: "icon_txt"
            }, "\u8bc4\u8bba")) : a >= 10 && a < 100 ? t = r.a.createElement("a", {
                className: "mod_btn_comment btn_comment99",
                onClick: l
            }, r.a.createElement("span", {
                className: "btn_comment__numbers"
            }, r.a.createElement("span", {
                className: "btn_comment__numb10000"
            }, a)), r.a.createElement("span", {
                className: "icon_txt"
            }, "\u8bc4\u8bba")) : a >= 100 && a < 1e3 ? t = r.a.createElement("a", {
                className: "mod_btn_comment btn_comment99",
                onClick: l
            }, r.a.createElement("span", {
                className: "btn_comment__numbers"
            }, r.a.createElement("i", {
                className: "btn_comment__numb btn_comment__numb_9"
            }), r.a.createElement("i", {
                className: "btn_comment__numb btn_comment__numb_9"
            }), r.a.createElement("i", {
                className: "btn_comment__numb btn_comment__numb_add"
            })), r.a.createElement("span", {
                className: "icon_txt"
            }, "\u8bc4\u8bba")) : a >= 1e3 && a < 1e4 ? t = r.a.createElement("a", {
                className: "mod_btn_comment btn_comment99",
                onClick: l
            }, r.a.createElement("span", {
                className: "btn_comment__numbers"
            }, r.a.createElement("i", {
                className: "btn_comment__numb btn_comment__numb_9"
            }), r.a.createElement("i", {
                className: "btn_comment__numb btn_comment__numb_9"
            }), r.a.createElement("i", {
                className: "btn_comment__numb btn_comment__numb_9"
            }), r.a.createElement("i", {
                className: "btn_comment__numb btn_comment__numb_add"
            })), r.a.createElement("span", {
                className: "icon_txt"
            }, "\u8bc4\u8bba")) : a >= 1e4 && (t = r.a.createElement("a", {
                className: "mod_btn_comment btn_comment99",
                onClick: l
            }, r.a.createElement("span", {
                className: "btn_comment__numbers"
            }, r.a.createElement("span", {
                className: "btn_comment__numb10000"
            }, a < 1e5 ? "1w" : "10w"), r.a.createElement("i", {
                className: "btn_comment__numb btn_comment__numb_add"
            })), r.a.createElement("span", {
                className: "icon_txt"
            }, "\u8bc4\u8bba"))),
            t
        }
        );
        t.a = Object(a.memo)(l)
    },
    473: function(e, t, n) {},
    551: function(e, t, n) {},
    552: function(e, t, n) {},
    553: function(e, t, n) {},
    554: function(e, t, n) {},
    555: function(e, t, n) {}
}]);
