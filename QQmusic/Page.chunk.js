(window.webpackJsonp = window.webpackJsonp || []).push([[2], {
    0: function(e, n, t) {
        "use strict";
        t.d(n, "b", (function() {
            return r
        }
        )),
        t.d(n, "a", (function() {
            return a
        }
        ));
        var a, r = "/n/ryqq/";
        !function(e) {
            e.INDEX = "index",
            e.PLAYLIST_EDIT = "playlist_edit",
            e.SINGER_LIST = "singer_list",
            e.SINGER = "singer",
            e.TOPLIST = "toplist",
            e.MV_TOPLIST = "mv_toplist",
            e.PLAYLIST = "playlist",
            e.PLAYER = "player",
            e.PROFILE = "profile",
            e.ALBUM = "album",
            e.SONGE_DETAIL = "songDetail",
            e.CATEGORY = "category",
            e.MV_LIST = "mvList",
            e.MV = "mv",
            e.RADIO = "radio",
            e.PLAYER_RADIO = "player_radio",
            e.ALBUM_MALL = "album_mall",
            e.ALBUM_DETAIL = "albumDetail",
            e.SEARCH = "search",
            e.NOT_FOUND = "notfound",
            e.CMT_PAGE = "cmtpage",
            e.MSG_CENTER = "msgcenter"
        }(a || (a = {}))
    },
    107: function(e, n, t) {
        "use strict";
        n.a = function(e) {
            return Number(e) === Number(e)
        }
    },
    151: function(e, n, t) {
        t(152),
        e.exports = t(360)
    },
    16: function(e, n, t) {
        "use strict";
        t.d(n, "c", (function() {
            return O
        }
        )),
        t.d(n, "d", (function() {
            return D
        }
        )),
        t.d(n, "e", (function() {
            return S
        }
        )),
        t.d(n, "b", (function() {
            return y
        }
        ));
        var a, r, o, i, c = t(78), l = t.n(c), u = t(2), m = t.n(u), d = t(46), s = t(15), p = t(107), h = t(74), f = t(0);
        function A(e, n) {
            var t = Object.keys(e);
            if (Object.getOwnPropertySymbols) {
                var a = Object.getOwnPropertySymbols(e);
                n && (a = a.filter((function(n) {
                    return Object.getOwnPropertyDescriptor(e, n).enumerable
                }
                ))),
                t.push.apply(t, a)
            }
            return t
        }
        function E(e) {
            for (var n = 1; n < arguments.length; n++) {
                var t = null != arguments[n] ? arguments[n] : {};
                n % 2 ? A(Object(t), !0).forEach((function(n) {
                    m()(e, n, t[n])
                }
                )) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : A(Object(t)).forEach((function(n) {
                    Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(t, n))
                }
                ))
            }
            return e
        }
        t.d(n, "a", (function() {
            return f.a
        }
        )),
        t.d(n, "f", (function() {
            return f.b
        }
        ));
        var L = function(e, n) {
            return "ryqq.".concat(e).concat(n ? ".".concat(n) : "")
        }
          , T = (a = {},
        m()(a, f.a.INDEX, "/"),
        m()(a, f.a.PLAYLIST_EDIT, "playlist_edit"),
        m()(a, f.a.SINGER_LIST, "singer_list"),
        m()(a, f.a.SINGER, "singer"),
        m()(a, f.a.TOPLIST, "toplist"),
        m()(a, f.a.MV_TOPLIST, "mv_toplist"),
        m()(a, f.a.PLAYLIST, "playlist"),
        m()(a, f.a.PLAYER, "player"),
        m()(a, f.a.PROFILE, "profile"),
        m()(a, f.a.ALBUM, "album"),
        m()(a, f.a.SONGE_DETAIL, "songDetail"),
        m()(a, f.a.CATEGORY, "category"),
        m()(a, f.a.MV_LIST, "mvList"),
        m()(a, f.a.MV, "mv"),
        m()(a, f.a.RADIO, "radio"),
        m()(a, f.a.ALBUM_MALL, "album_mall"),
        m()(a, f.a.ALBUM_DETAIL, "albumDetail"),
        m()(a, f.a.SEARCH, "search"),
        m()(a, f.a.NOT_FOUND, "notfound"),
        m()(a, f.a.MSG_CENTER, "msg_center"),
        a)
          , I = (r = {},
        m()(r, f.a.INDEX, "//i.y.qq.com/n2/m/index.html"),
        m()(r, f.a.PLAYLIST_EDIT, ""),
        m()(r, f.a.SINGER_LIST, ""),
        m()(r, f.a.SINGER, "//i.y.qq.com/n2/m/share/details/singer.html"),
        m()(r, f.a.TOPLIST, "//i.y.qq.com/n2/m/share/details/toplist.html"),
        m()(r, f.a.MV_TOPLIST, ""),
        m()(r, f.a.PLAYLIST, "//i.y.qq.com/n2/m/share/details/taoge.html"),
        m()(r, f.a.PLAYER, ""),
        m()(r, f.a.PROFILE, "//i.y.qq.com/n2/m/share/profile_v2/index.html"),
        m()(r, f.a.ALBUM, ""),
        m()(r, f.a.SONGE_DETAIL, "//i.y.qq.com/v8/playsong.html"),
        m()(r, f.a.CATEGORY, ""),
        m()(r, f.a.MV_LIST, ""),
        m()(r, f.a.MV, "//i.y.qq.com/n2/m/share/details/mv.html"),
        m()(r, f.a.ALBUM_MALL, "//i.y.qq.com/n2/m/album/mall/index.html"),
        m()(r, f.a.ALBUM_DETAIL, "//i.y.qq.com/n2/m/share/details/album.html"),
        m()(r, f.a.SEARCH, "//i.y.qq.com/n2/m/index.html"),
        m()(r, f.a.NOT_FOUND, ""),
        r)
          , b = (o = {},
        m()(o, f.a.INDEX, {}),
        m()(o, f.a.PLAYLIST_EDIT, {}),
        m()(o, f.a.SINGER_LIST, {}),
        m()(o, f.a.SINGER, {
            ADTAG: L(f.a.SINGER),
            source: "ydetail",
            singermid: ""
        }),
        m()(o, f.a.TOPLIST, {
            ADTAG: L(f.a.TOPLIST),
            type: 0,
            id: 0
        }),
        m()(o, f.a.MV_TOPLIST, {}),
        m()(o, f.a.PLAYLIST, {
            ADTAG: L(f.a.PLAYLIST),
            id: ""
        }),
        m()(o, f.a.PLAYER, {}),
        m()(o, f.a.PROFILE, {
            userid: "",
            ADTAG: L(f.a.PROFILE)
        }),
        m()(o, f.a.ALBUM, {}),
        m()(o, f.a.SONGE_DETAIL, {
            ADTAG: L(f.a.SONGE_DETAIL),
            songmid: ""
        }),
        m()(o, f.a.CATEGORY, {}),
        m()(o, f.a.MV_LIST, {}),
        m()(o, f.a.MV, {
            ADTAG: L(f.a.MV),
            vid: ""
        }),
        m()(o, f.a.ALBUM_MALL, {}),
        m()(o, f.a.ALBUM_DETAIL, {
            ADTAG: L(f.a.ALBUM_DETAIL),
            source: "ydetail",
            albummid: ""
        }),
        m()(o, f.a.SEARCH, {}),
        m()(o, f.a.NOT_FOUND, {}),
        o)
          , _ = (i = {},
        m()(i, f.a.INDEX, ""),
        m()(i, f.a.PLAYLIST_EDIT, ""),
        m()(i, f.a.SINGER_LIST, ""),
        m()(i, f.a.SINGER, ""),
        m()(i, f.a.TOPLIST, ""),
        m()(i, f.a.MV_TOPLIST, ""),
        m()(i, f.a.PLAYLIST, ""),
        m()(i, f.a.PLAYER, ""),
        m()(i, f.a.PROFILE, ""),
        m()(i, f.a.ALBUM, ""),
        m()(i, f.a.SONGE_DETAIL, "#webchat_redirect"),
        m()(i, f.a.CATEGORY, ""),
        m()(i, f.a.MV_LIST, ""),
        m()(i, f.a.MV, ""),
        m()(i, f.a.ALBUM_MALL, ""),
        m()(i, f.a.ALBUM_DETAIL, ""),
        m()(i, f.a.SEARCH, ""),
        m()(i, f.a.NOT_FOUND, ""),
        i)
          , v = function(e) {
            var n = e.replace(/^(http:)?\/\//i, "https://");
            return /^https:\/\//i.test(n) || (n = "https://y.qq.com".concat(n)),
            n
        }
          , S = function(e) {
            return e === f.a.INDEX ? T[f.a.INDEX] : "".concat(f.b).concat(T[e] || "")
        }
          , D = function(e, n, t) {
            var a = S(e);
            return "string" === typeof n || "number" === typeof n ? a = "".concat(a, "/").concat(n) : n && n.length > 0 && (a = "".concat(a, "/").concat(n.join("/"))),
            s.k.addParam(t, a)
        }
          , O = function(e, n) {
            var t = I[e]
              , a = s.k.addParam(n, t) + (_[e] || "");
            return v(a.replace(/[&?]$/, ""))
        }
          , P = function(e, n, t) {
            if (!I[e] || e === f.a.NOT_FOUND)
                return "";
            var a = ""
              , r = {};
            switch (e) {
            case f.a.INDEX:
                if (!s.d.isBrowser)
                    return "";
                var o = n.type
                  , i = n.id
                  , c = void 0 === i ? "0" : i
                  , u = n.mid
                  , m = void 0 === u ? "" : u
                  , d = n.p
                  , h = void 0 === d ? "" : d
                  , A = n.songtype
                  , T = void 0 === A ? "0" : A
                  , v = n.shareuin
                  , S = void 0 === v ? "" : v
                  , D = n.w
                  , O = void 0 === D ? "" : D;
                n.ADTAG,
                n.tab;
                switch (o) {
                case "album":
                    if (!c && !m)
                        return "";
                    e = f.a.ALBUM_DETAIL,
                    r = {
                        albumid: c,
                        albummid: m,
                        ADTAG: L(f.a.INDEX, f.a.ALBUM_DETAIL)
                    };
                    break;
                case "singer":
                    if (!c && !m)
                        return "";
                    e = f.a.SINGER,
                    r = {
                        singerid: c,
                        singermid: m,
                        ADTAG: L(f.a.INDEX, f.a.SINGER)
                    };
                    break;
                case "song":
                    if (!c && !m)
                        return "";
                    e = f.a.SONGE_DETAIL,
                    r = {
                        songid: c,
                        songmid: m,
                        songtype: T,
                        shareuin: S,
                        ADTAG: L(f.a.INDEX, f.a.SONGE_DETAIL)
                    };
                    break;
                case "topic":
                    if (!c)
                        return "";
                    a = "//y.qq.com/w/topic.html",
                    r = {
                        id: c,
                        ADTAG: L(f.a.INDEX, "topic")
                    };
                    break;
                case "taoge":
                    if (!c)
                        return "";
                    e = f.a.PLAYLIST,
                    r = {
                        id: c,
                        ADTAG: L(f.a.INDEX, f.a.PLAYLIST)
                    };
                    break;
                case "toplist":
                    var P = decodeURIComponent(h);
                    if (!/_/gi.test(P))
                        return "";
                    var g = P.split("_")
                      , N = l()(g, 2)
                      , y = N[0]
                      , R = N[1];
                    R = parseInt(R || "2", 10),
                    y = y || "top",
                    e = f.a.TOPLIST,
                    r = {
                        id: (R || "2").toString(),
                        type: "global" === y ? "1" : "0",
                        ADTAG: L(f.a.INDEX, f.a.TOPLIST)
                    };
                    break;
                case "mymusic":
                    var G = decodeURIComponent(h)
                      , k = s.k.getParam("uin", G)
                      , M = s.k.getParam("dirid", G);
                    if (k && M) {
                        var q = {
                            str2Hex: function(e) {
                                for (var n = encodeURIComponent(e), t = "", a = 0, r = n.length; a < r; ) {
                                    var o = "";
                                    if ("%" === n.charAt(a))
                                        o = n.substring(a, a + 3),
                                        a += 3;
                                    else {
                                        var i = n.charCodeAt(a);
                                        o = "%".concat(i.toString(16)),
                                        a++
                                    }
                                    t += o
                                }
                                return t.replace(/%/g, "").toUpperCase()
                            }
                        }.str2Hex(k);
                        a = "//y.qq.com/w/myalbum.html",
                        r = {
                            ADTAG: L(f.a.INDEX, "mymusic"),
                            bu: q,
                            dirid: M
                        }
                    } else
                        a = "//y.qq.com/m/index.html",
                        r = {
                            ADTAG: L(f.a.INDEX, "mymusic")
                        };
                    break;
                case "soso":
                    var C = decodeURIComponent(O);
                    a = "//y.qq.com/m/index.html#search/".concat(C),
                    r = {
                        ADTAG: L(f.a.INDEX, "soso")
                    }
                }
                break;
            case f.a.SINGER:
                r.singermid = t.params.id;
                break;
            case f.a.TOPLIST:
                if (!t.params.id && !n.id)
                    return "";
                r.id = t.params.id || n.id,
                n.type && (r.type = n.type);
                break;
            case f.a.PLAYLIST:
                r.id = t.params.id,
                (n.hosteuin || n.hostuin) && (r.hosteuin = n.hosteuin || n.hostuin);
                break;
            case f.a.SONGE_DETAIL:
                var U = t.params.mid
                  , w = Object(p.a)(U) ? "" : U
                  , x = Object(p.a)(U) ? parseInt(U, 10) : 0;
                r.songmid = w,
                r.songid = x.toString(),
                r.songtype = n.songtype || "0";
                break;
            case f.a.MV:
                r.vid = t.params.mid;
                break;
            case f.a.ALBUM_DETAIL:
                r.albummid = t.params.id;
                break;
            case f.a.PROFILE:
                if (!n.uin && !n.userid)
                    return "";
                r.userid = n.userid || n.uin;
                break;
            default:
                r = t.params
            }
            Object.keys(r).forEach((function(e) {
                void 0 !== r[e] && null !== r[e] || delete r[e]
            }
            ));
            var Y = E(E({}, b[e] || {}), r);
            return (a = a || I[e]) ? (s.k.addParam(Y, a) + (_[e] || "")).replace(/[&?]$/, "") : ""
        }
          , g = function(e, n) {
            if (e !== f.a.INDEX)
                return "";
            var t = ""
              , a = {}
              , r = n.type
              , o = n.id
              , i = void 0 === o ? "0" : o
              , c = n.mid
              , u = void 0 === c ? "" : c
              , m = n.p
              , d = void 0 === m ? "" : m
              , p = n.songtype
              , h = void 0 === p ? "0" : p
              , A = n.w
              , E = void 0 === A ? "" : A
              , T = n.ADTAG
              , I = void 0 === T ? "" : T
              , b = n.tab
              , _ = void 0 === b ? "" : b
              , v = n.uin
              , S = void 0 === v ? "" : v;
            switch (r) {
            case "album":
                t = i || u ? D(f.a.ALBUM_DETAIL, u || i) : D(f.a.ALBUM_MALL),
                a = {
                    ADTAG: I
                };
                break;
            case "singer":
                t = i || u ? D(f.a.SINGER, u || i) : D(f.a.SINGER_LIST),
                a = {
                    ADTAG: I
                };
                break;
            case "song":
                if (!i && !u)
                    return "";
                t = D(f.a.SONGE_DETAIL, u || i),
                a = {
                    songtype: h,
                    ADTAG: I
                };
                break;
            case "topic":
                if (!i)
                    return "";
                t = "//y.qq.com/w/topic.html",
                a = {
                    id: i,
                    ADTAG: L(f.a.INDEX, "topic")
                };
                break;
            case "taoge":
                if (!i)
                    return "";
                t = D(f.a.PLAYLIST, i),
                a = {
                    ADTAG: I
                };
                break;
            case "toplist":
                var O = decodeURIComponent(d);
                if (O && !/top_index/.test(O)) {
                    var P = (O = O.replace(/\.html/g, "")).split("_")
                      , g = l()(P, 2)
                      , N = (g[0],
                    g[1]);
                    t = D(f.a.TOPLIST, N || "4")
                } else
                    t = D(f.a.TOPLIST, i || "4");
                a = {
                    ADTAG: I
                };
                break;
            case "mymusic":
                var y = decodeURIComponent(d);
                t = !y || "songlist_import.html" !== y && "list_recover.html" !== y ? y && "mymusic_albumlist.html" === y ? D(f.a.PROFILE, "create") : D(f.a.PROFILE) : "//y.qq.com/portal/".concat(y, "?tab=").concat(_),
                a = {
                    ADTAG: I,
                    uin: S
                };
                break;
            case "soso":
                var R = decodeURIComponent(E);
                t = D(f.a.SEARCH),
                a = {
                    ADTAG: I,
                    w: R
                };
                break;
            case "down":
                t = "//y.qq.com/download/index.html",
                a = {
                    ADTAG: I
                };
                break;
            case "qplay":
                var G = decodeURIComponent(d);
                t = G ? "//y.qq.com/y/static/qplay/".concat(G) : "//y.qq.com/y/static/qplay/qplay_coop.html",
                a = {
                    ADTAG: I
                }
            }
            return t ? (Object.keys(a).forEach((function(e) {
                void 0 !== a[e] && null !== a[e] && "" !== a[e] || delete a[e]
            }
            )),
            s.k.addParam(a, t).replace(/[&?]$/, "")) : ""
        }
          , N = function(e) {
            var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}
              , t = arguments.length > 2 && void 0 !== arguments[2] && arguments[2]
              , a = Object(d.a)(h.a, e)
              , r = a.match
              , o = a.activeRoute
              , i = o.chunkName || f.a.NOT_FOUND;
            return t ? P(i, n, r) : g(i, n)
        }
          , y = function(e) {
            if (s.d.isBrowser) {
                var n = s.k.getParams(location.href)
                  , t = navigator.userAgent.match(/Android|iPhone|iPod/i) && "1" !== n.no_redirect
                  , a = N(location.pathname, E(E({}, n), e), t);
                if (a)
                    return window.location.replace(v(a)),
                    !0;
                if (/^http:/.test(location.href))
                    return window.location.replace(v(location.href)),
                    !0
            }
            return !1
        }
    },
    354: function(e, n, t) {},
    360: function(e, n, t) {
        "use strict";
        t.r(n);
        var a, r = t(108), o = t.n(r), i = t(76), c = t.n(i), l = t(3), u = t.n(l), m = t(109), d = t.n(m), s = t(77), p = t(11), h = (t(341),
        t(46)), f = t(75), A = t(15), E = t(74).a, L = t(16);
        !function(e) {
            var n = e.normalizePathName = function(e) {
                var n, t = location.pathname, a = t, r = location.search || "", o = null === (n = document) || void 0 === n ? void 0 : n.createElement("a");
                if (o && e && (o.href = e),
                [L.a.SINGER, L.a.SONGE_DETAIL, L.a.MV, L.a.PLAYLIST, L.a.PLAYLIST_EDIT, L.a.TOPLIST, L.a.MV_TOPLIST, L.a.ALBUM_DETAIL].some((function(e) {
                    return t.includes(Object(L.d)(e))
                }
                ))) {
                    var i = Object(h.a)(E, t).match;
                    a = i.path,
                    r = "".concat(r ? "".concat(r, "&") : "?").concat(A.k.param(i.params)),
                    o && o.href === location.href && (o.href = o.origin + a + r)
                }
                return "/" === a && (a = "".concat(L.f, "index.html")),
                {
                    pathname: a,
                    search: r,
                    file_host: (null === o || void 0 === o ? void 0 : o.host) || "",
                    file_pathname: (null === o || void 0 === o ? void 0 : o.pathname) || "",
                    file_search: (null === o || void 0 === o ? void 0 : o.search) || ""
                }
            }
            ;
            e.mannualSend = function(e, t) {
                var a = n()
                  , r = a.pathname
                  , o = a.search;
                Object(f.a)("web_error", {
                    host: location.host,
                    pathname: r,
                    search: o,
                    protocol: location.protocol,
                    hash: location.hash,
                    message: e,
                    ext_str: t || ""
                })
            }
        }(a || (a = {})),
        setTimeout((function() {
            Object(f.b)({
                _os: A.d.client,
                _app: A.d.client,
                _app_version: A.d.version && A.d.version.toString(),
                fqm_id: "7642c64d-5680-42a8-b8be-b2a114021486"
            })
        }
        ), 0);
        window.addEventListener("error", (function(e) {
            var n = e.message
              , t = e.filename
              , r = e.lineno
              , o = e.colno
              , i = e.error
              , c = a.normalizePathName(t)
              , l = c.pathname
              , u = c.search
              , m = c.file_host
              , d = c.file_pathname
              , s = c.file_search;
            !function(e, n) {
                var t = new XMLHttpRequest;
                t.onreadystatechange = function() {
                    if (4 === t.readyState && (t.status >= 200 && t.status < 300 || 304 == t.status)) {
                        var e = t.getResponseHeader("X-Server-Ip") || t.getResponseHeader("x-server-ip") || "";
                        n(e)
                    }
                }
                ,
                t.open("head", e),
                t.send()
            }(t, (function(e) {
                Object(f.a)("web_error", {
                    host: location.host,
                    pathname: l,
                    search: u,
                    protocol: location.protocol,
                    hash: location.hash,
                    message: n,
                    stack: i && i.stack || "",
                    line: "".concat(r),
                    column: "".concat(o),
                    file_host: m,
                    file_pathname: d,
                    file_search: s,
                    file_ip: e
                })
            }
            ))
        }
        ));
        t(354);
        window.QMFE_SPD_RATE = 1,
        Object(L.b)(),
        function() {
            var e = o()(c.a.mark((function e() {
                var n;
                return c.a.wrap((function(e) {
                    for (; ; )
                        switch (e.prev = e.next) {
                        case 0:
                            return e.next = 2,
                            Object(h.c)(E);
                        case 2:
                            n = e.sent,
                            d.a[window.__USE_SSR__ ? "hydrate" : "render"](u.a.createElement(s.a, null, u.a.createElement(p.Switch, null, n.map((function(e) {
                                var n = e.path
                                  , t = e.exact
                                  , a = (0,
                                e.Component)()
                                  , r = Object(h.b)(a);
                                return u.a.createElement(p.Route, {
                                    exact: t,
                                    key: n,
                                    path: n,
                                    render: function() {
                                        return u.a.createElement(r, null)
                                    }
                                })
                            }
                            )), u.a.createElement(p.Redirect, {
                                to: Object(L.e)(L.a.NOT_FOUND)
                            }))), document.getElementById("app"));
                        case 5:
                        case "end":
                            return e.stop()
                        }
                }
                ), e)
            }
            )));
            return function() {
                return e.apply(this, arguments)
            }
        }()()
    },
    74: function(e, n, t) {
        "use strict";
        var a = t(3)
          , r = t.n(a)
          , o = t(9)
          , i = t.n(o)
          , c = t(0)
          , l = [{
            path: "(/|/n|".concat(c.b.slice(0, c.b.length - 1), "|").concat(c.b, "index.html|").concat(c.b, "static/index.html|/ryqq/index.html)"),
            exact: !0,
            chunkName: c.a.INDEX,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(8)]).then(t.bind(null, 374))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "playlist_edit/:dirid?"),
            exact: !1,
            chunkName: c.a.PLAYLIST_EDIT,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(17)]).then(t.bind(null, 386))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "singer_list"),
            exact: !0,
            chunkName: c.a.SINGER_LIST,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(23)]).then(t.bind(null, 384))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "singer/:id"),
            chunkName: c.a.SINGER,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(22)]).then(t.bind(null, 375))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "toplist/:id"),
            exact: !0,
            chunkName: c.a.TOPLIST,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(25)]).then(t.bind(null, 391))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "mv_toplist/:id?"),
            exact: !0,
            chunkName: c.a.MV_TOPLIST,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(12)]).then(t.bind(null, 389))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "playlist/:id"),
            exact: !0,
            chunkName: c.a.PLAYLIST,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(16)]).then(t.bind(null, 370))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "player"),
            exact: !0,
            chunkName: c.a.PLAYER,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(14)]).then(t.bind(null, 376))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "profile/:tab?/:sub?"),
            chunkName: c.a.PROFILE,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(18)]).then(t.bind(null, 373))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "album"),
            exact: !0,
            chunkName: c.a.ALBUM,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(3)]).then(t.bind(null, 387))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "songDetail/:mid"),
            exact: !0,
            chunkName: c.a.SONGE_DETAIL,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(24)]).then(t.bind(null, 383))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "category"),
            exact: !0,
            chunkName: c.a.CATEGORY,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(6)]).then(t.bind(null, 377))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "player_radio"),
            exact: !0,
            chunkName: c.a.PLAYER_RADIO,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(15)]).then(t.bind(null, 378))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "radio"),
            exact: !0,
            chunkName: c.a.RADIO,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(19)]).then(t.bind(null, 388))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "mvList"),
            exact: !0,
            chunkName: c.a.MV_LIST,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(11)]).then(t.bind(null, 390))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "mv/:mid"),
            exact: !0,
            chunkName: c.a.MV,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(10)]).then(t.bind(null, 385))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "album_mall"),
            exact: !0,
            chunkName: c.a.ALBUM_MALL,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(5)]).then(t.bind(null, 381))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "albumDetail/:id"),
            exact: !0,
            chunkName: c.a.ALBUM_DETAIL,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(4)]).then(t.bind(null, 380))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "search"),
            exact: !0,
            chunkName: c.a.SEARCH,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(21)]).then(t.bind(null, 379))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "cmtpage"),
            exact: !0,
            chunkName: c.a.CMT_PAGE,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(7)]).then(t.bind(null, 371))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "notfound"),
            chunkName: c.a.NOT_FOUND,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(13)]).then(t.bind(null, 372))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }, {
            path: "".concat(c.b, "msg_center"),
            chunkName: c.a.MSG_CENTER,
            Component: function() {
                return i()({
                    loader: function() {
                        return Promise.all([t.e(0), t.e(1), t.e(9)]).then(t.bind(null, 382))
                    },
                    loading: function() {
                        return r.a.createElement("div")
                    }
                })
            }
        }];
        n.a = l
    }
}, [[151, 20, 0]]]);
