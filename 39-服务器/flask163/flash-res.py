from flask import Flask, render_template,request,send_file,Response
import get163music
import time



app = Flask(__name__)

@app.route('/')
def index():
    return render_template('/index.html')

@app.route('/getsong',methods=['GET'])
def getsong():
    song_name = request.args.get('song_name')
    print(song_name)
    # 假设您的下载函数返回一个文件路径
    file_path = get163music.download(str(get163music.search_song(song_name)),song_name)
    print(file_path)
    #使用 send_file 函数发送文件
    return send_file("C:/Users/roysx/Pictures/Desktop/"+song_name+".mp3", as_attachment=False)
    #return "test"

@app.route('/test')
def download():
    return send_file("C:/Users/roysx/Pictures/Desktop/天外来物.mp3", as_attachment=True)

def file_send(file_path): 
    with open(file_path, 'rb') as f:
        while 1:
            data = f.read(20 * 1024 * 1024)  # per 20M
            if not data:
                break
            yield data

@app.route("/download2")
def download2():
    file_name = "C:/Users/roysx/Pictures/Desktop/xxxxx.jpg"
    response = Response(file_send(file_name), content_type='image/jpeg')
    response.headers["Content-disposition"] = f'attachment; filename={"xxxxx.jpg"}'
    return response


@app.route("/download3")
def download3():
    file_name = "C:/Users/roysx/Pictures/Desktop/rest.xlsx"
    response = Response(file_send(file_name), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response.headers["Content-disposition"] = f'attachment; filename={"rest.xlsx"}'
    return response

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5055)

# nohup python3 -u flash-res.py >163flask.log 2>&1 &
