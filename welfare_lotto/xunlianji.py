import numpy as np

# 创建一个二维数组
data = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

# 生成一个随机的训练集数组
index = np.random.choice(3, 2, replace=False)
train_data = data[index]
print(index)

print(train_data)

import pandas as pd

# 创建一个数据框
data = {'name': ['John', 'Mary', 'Peter', 'Bob'], 
        'age': [25, 30, 35, 40], 
        'gender': ['M', 'F', 'M', 'M']}

df = pd.DataFrame(data)

# 生成一个随机的训练集数据框
index = pd.Index(np.random.choice(4, 2, replace=False))
print(index)
train_df = df.iloc[index]

print(train_df)

from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split

# 加载iris数据集
iris = load_iris()

# 划分训练集和测试集
x_train, x_test, y_train, y_test = train_test_split(
    iris.data, iris.target, test_size=0.2, random_state=0)

print(x_train.shape)
print(y_train.shape)



