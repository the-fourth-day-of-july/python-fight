import scrapy
import bs4
from ..items import Item


class Spider(scrapy.Spider):
    name='damai'
    #name 是定义爬虫的名字，是唯一标识，启动爬虫时，会用到这个名字
    allowed_domains = []
    #定义允许爬虫爬取的网址域名，不需要加https://，如果网址的域名不在整个列表里，会被过滤掉
    #可以限制关联爬取的url，一定在允许的域名下
    start_urls=[]
    #定义起始网址，定义爬虫从哪个网址开始抓取，，allowed_domains的设置对start_urls里的网址没有影响
    def parse(self,response):
    #默认处理response的方法
        html = bs4.BeautifulSoup(response.text,'html.parser')
        data = html.find_all('',class_='')
        #根据页面提取标签，后续可以遍历获取信息
        for i in data:
            item = Item()
            #实例化Item类
            item['title'] = ''
            item['publish'] = ''
            item['score'] = ''
            #提取出信息，并把整个数据放回Item类的属性里
            yield item
            #把获得的item传递给引擎
