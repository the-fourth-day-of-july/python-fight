import os           # 创建文件夹, 文件是否存在
import time         # time 计时
import pickle       # 保存和读取cookie实现免登陆的一个工具
from time import sleep
from selenium import webdriver  # 操作浏览器的工具
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.alert import Alert


# 大麦网主页
damai_url = 'https://www.damai.cn/'
# 登录
login_url = 'https://passport.damai.cn/login?ru=https%3A%2F%2Fwww.damai.cn%2F'
# 抢票目标页
target_url = 'https://detail.damai.cn/item.htm?spm=a2oeg.home.card_0.ditem_3.5edd23e17JgVkE&id=742999429284'

# 创建一个 Firefox 配置实例
firefox_options = webdriver.FirefoxOptions()
# 设置请求头信息
firefox_options.set_capability("moz:firefoxOptions", {"extraHeaders": {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/118.0"}})
#firefox_options.add_argument("-headless")
firefox_options.add_argument('blink-settings=imagesEnabled=false')  # 不加载图片, 提升速度

Cookie={
    "XSRF-TOKEN": "d4f045a5-131b-4462-b07a-86befb2ccf25",
    "destCity": "%u5317%u4eac",
    "cna": "8be7HX8cXSABASQJinRgo4ZG",
    "isg": "BJ2dqVqrpAiFAkAkBuA4xJDIr3mXutEMg0nMZV9i2fQjFr1IJwrh3GuEQIoQzenE",
    "l": "fBOio5ElP_L_sjZ2BOfanurza77OSIOYYuPzaNbMi9fPOS1p5JZdW13J7xT9C31VF6uBR3o5gAWyBeYBqIVHavVy51xKbckmndLHR35..",
    "tfstk": "dqj2QH1r_oE4C2tf5Exw4VIAg18vfnVIuGO6IOXMhIAmcmGGz6Cwl-QfIC7wZnYOlCOX_hWOTZPQAk6AHhLgOWZBN-BvXHg1QmECHtK9jWNQAkiwE0zlUyJmuce95WX6ZiLycd6Qk9RrjcWp3UA3vQoijgJqOBXwWfpufSIOuf0Moc9yO8yzHncah",
    "xlly_s": "1",
    "_samesite_flag_": "true",
    "damai.cn_user_new": "cxIjneFbBVj0EQMC2Van1VeAxbia8W6FUiKYv%2FBS3s%2F6UrLqf96EXwCR0C50oh9jGxb2%2BRjuqig%3D",
    "h5token": "aae8286282b74fbda7fdeb451f527dbb_1_1",
    "damai_cn_user": "cxIjneFbBVj0EQMC2Van1VeAxbia8W6FUiKYv%2FBS3s%2F6UrLqf96EXwCR0C50oh9jGxb2%2BRjuqig%3D",
    "loginkey": "aae8286282b74fbda7fdeb451f527dbb_1_1",
    "user_id": "229145264",
    "_m_h5_tk": "58e789f996a51edbd4b98c7f4985713c_1697968154067",
    "_m_h5_tk_enc": "1759dc5600ac60c2886cb6a9330652c1"
}

#账号
#密码


def getUrl():
    driver = webdriver.Firefox(options=firefox_options)
    driver.get(target_url)


    # try:  
    #     alert = Alert(driver)  
    #     alert.accept()  # 或者你可以使用 alert.dismiss() 来关闭对话框  
    # except Exception as e:  
    #     print(f"No alert present: {e}")


    driver.delete_all_cookies()

    # 设置cookies
    for cookie_name, cookie_value in Cookie.items():
        driver.add_cookie({'name': cookie_name, 'value': cookie_value})

    # 刷新页面，确保cookies生效
    driver.refresh()

    print(driver.title)

getUrl()
